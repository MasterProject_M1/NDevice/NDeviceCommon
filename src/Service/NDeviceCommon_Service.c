#include "../../include/NDeviceCommon.h"

// --------------------------------
// namespace NDeviceCommon::Service
// --------------------------------

/**
 * Build http response with parser output list
 * Will replace 0xFF by '.'
 *
 * @param httpResponseCode
 * 		The http response code
 * @param parserOutputList
 * 		The parser output list
 * @param clientID
 * 		The client ID
 *
 * @return the http response
 */
__ALLOC NReponseHTTP *NDeviceCommon_Service_GenerateJsonHTTPResponseWithParserOutputList( NHTTPCode httpResponseCode,
	__WILLBEOWNED NParserOutputList *parserOutputList,
	NU32 clientID )
{
	// HTTP Response
	__OUTPUT NReponseHTTP *response;

	// Json
	char *json;

	// Build http response
	if( !( response = NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Construire( httpResponseCode,
		clientID ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Destroy
		NParser_Output_NParserOutputList_Destroy( &parserOutputList );

		// Quit
		return NULL;
	}

	// Export parser output list to json
	if( !( json = NJson_Engine_Builder_Build( parserOutputList ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_SYNTAX );

		// Destroy
		NParser_Output_NParserOutputList_Destroy( &parserOutputList );

		// Quit
		return NULL;
	}

	// Correct json (when '.' is a problem)
	NLib_Chaine_Remplacer( json,
		NDEVICECOMMON_TEMPORARY_POINT_CHARACTER_REPLACEMENT_CHARACTER,
		'.' );

	// Save content
	NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_DefinirFichier2( response,
		json );

	// Add json content type
	NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_AjouterAttribut( response,
		NMOT_CLEF_REPONSE_HTTP_CONTENT_TYPE,
		NHTTP_Commun_HTTP_Mimetype_NTypeFlux_ObtenirType( NTYPE_FLUX_JSON ) );

	// Free
	NFREE( json );

	// Destroy
	NParser_Output_NParserOutputList_Destroy( &parserOutputList );

	// OK
	return response;
}
