#define NDEVICECOMMON_SERVICE_AUTHENTICATION_NAUTHENTICATIONSERVICETYPE_INTERNE
#include "../../../include/NDeviceCommon.h"

// -----------------------------------------------------------------------
// enum NDeviceCommon::Service::Authentication::NAuthenticationServiceType
// -----------------------------------------------------------------------

/**
 * Get service name
 *
 * @param serviceType
 * 		The service type
 *
 * @return the service name
 */
const char *NDeviceCommon_Service_Authentication_NAuthenticationServiceType_GetName( NAuthenticationServiceType serviceType )
{
	return NAuthenticationServiceTypeText[ serviceType ];
}

/**
 * Find service type
 *
 * @param authenticationManager
 * 		The authentication manager
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in reeuqested element
 * @param authenticationEntryIndex
 * 		The authentication entry index (output)
 *
 * @return the service type
 */
NAuthenticationServiceType NDeviceCommon_Service_Authentication_NAuthenticationServiceType_FindServiceType( __MUSTBEPROTECTED NAuthenticationManager *authenticationManager,
	const char *requestedElement,
	NU32 *cursor,
	__OUTPUT NU32 *authenticationEntryIndex )
{
	// Buffer
	char *buffer;

	// IP Test
	NU32 ipTest;

	// Buffer
	NU32 bufferCursor = 0;

	// IP/Hostname part
	char *hostnameIP;

	// Port
	char *readPort;
	NU32 port;

	// Service
	NAuthenticationServiceType serviceType = (NAuthenticationServiceType)0;

	// Read element
	if( !( buffer = NLib_Chaine_LireJusqua( requestedElement,
		'/',
		cursor,
		NFALSE ) ) )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Quit
		return NAUTHENTICATION_SERVICE_TYPES;
	}

	// Look for
	for( ; serviceType < NAUTHENTICATION_SERVICE_TYPES; serviceType++ )
		switch( serviceType )
		{
			case NAUTHENTICATION_SERVICE_TYPE_AUTHENTICATION_ENTRY:
				// Check buffer
				if( !NLib_Chaine_EstChaineContient( buffer,
					':' ) )
					// Ignore
					break;

				// Read ip/hostname part
				if( !( hostnameIP = NLib_Chaine_LireJusqua( buffer,
					':',
					&bufferCursor,
					NFALSE ) ) )
				{
					// Notify
					NOTIFIER_ERREUR( NERREUR_SYNTAX );

					// Free
					NFREE( buffer );

					// Quit
					return NAUTHENTICATION_SERVICE_TYPES;
				}

				// Read port
				if( !( readPort = NLib_Chaine_LireJusqua( buffer,
					'\0',
					&bufferCursor,
					NFALSE ) ) )
				{
					// Notify
					NOTIFIER_ERREUR( NERREUR_SYNTAX );

					// Free
					NFREE( hostnameIP );
					NFREE( buffer );

					// Quit
					return NAUTHENTICATION_SERVICE_TYPES;
				}

				// Convert port
				port = (NU32)strtol( readPort,
					NULL,
					10 );

				// Free
				NFREE( readPort );

				// Is it an ip?
				if( !NLib_Chaine_EstChaineContientLettre( hostnameIP )
					&& NLib_Module_Reseau_IP_DivideIP( hostnameIP,
					NPROTOCOLE_IP_IPV4,
					(NU8*)&ipTest ) )
					// Find ip:port
					*authenticationEntryIndex = NDeviceCommon_Authentication_NAuthenticationManager_FindAuthenticationEntryIndexByIP( authenticationManager,
						hostnameIP,
						port );
				// Is it an hostname?
				else
					// Find hostname:port
					*authenticationEntryIndex = NDeviceCommon_Authentication_NAuthenticationManager_FindAuthenticationEntryIndexByHostname( authenticationManager,
						hostnameIP,
						port );

				// Free
				NFREE( hostnameIP );
				NFREE( buffer );

				// Check index
				if( ( *authenticationEntryIndex == NERREUR ) )
				{
					// Notify
					NOTIFIER_ERREUR( NERREUR_SYNTAX );

					// Quit
					return NAUTHENTICATION_SERVICE_TYPES;
				}

				// Found it
				return serviceType;

			default:
				// Check buffer
				if( NLib_Chaine_Comparer( buffer,
					NAuthenticationServiceTypeText[ serviceType ],
					NTRUE,
					0 ) )
				{
					// Free
					NFREE( buffer );

					// Found it
					return serviceType;
				}
				break;
		}

	// Free
	NFREE( buffer );

	// Not found
	return NAUTHENTICATION_SERVICE_TYPES;
}

