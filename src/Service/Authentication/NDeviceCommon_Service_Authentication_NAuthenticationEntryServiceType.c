#define NDEVICECOMMON_SERVICE_AUTHENTICATION_NAUTHENTICATIONENTRYSERVICETYPE_INTERNE
#include "../../../include/NDeviceCommon.h"

/**
 * Get service name
 *
 * @param serviceType
 * 		The service type
 *
 * @return the service name
 */
const char *NDeviceCommon_Service_Authentication_NAuthenticationEntryServiceType_GetName( NAuthenticationEntryServiceType serviceType )
{
	return NAuthenticationEntryServiceTypeText[ serviceType ];
}

/**
 * Find service type
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		Cursor in the requested element
 *
 * @return the service type or NAUTHENTICATION_ENTRY_SERVICE_TYPES
 */
NAuthenticationEntryServiceType NDeviceCommon_Service_Authentication_NAuthenticationEntryServiceType_FindServiceType( const char *requestedElement,
	NU32 *cursor )
{
	// Output
	__OUTPUT NAuthenticationEntryServiceType output = (NAuthenticationEntryServiceType)0;

	// Buffer
	char *buffer;

	// Read
	if( !( buffer = NLib_Chaine_LireJusqua( requestedElement,
		'/',
		cursor,
		NFALSE ) ) )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Quit
		return NAUTHENTICATION_ENTRY_SERVICE_TYPES;
	}

	// Look for
	for( ; output < NAUTHENTICATION_ENTRY_SERVICE_TYPES; output++ )
		// Check
		if( NLib_Chaine_Comparer( buffer,
			NAuthenticationEntryServiceTypeText[ output ],
			NTRUE,
			0 ) )
		// Found it
		{
			NFREE( buffer );

			// OK
			return output;
		}

	// Free
	NFREE( buffer );

	// Not found
	return NAUTHENTICATION_ENTRY_SERVICE_TYPES;
}

