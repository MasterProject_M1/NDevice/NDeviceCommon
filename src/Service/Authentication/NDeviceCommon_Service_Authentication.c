#include "../../../include/NDeviceCommon.h"

// ------------------------------------------------
// namespace NDeviceCommon::Service::Authentication
// ------------------------------------------------

/**
 * Extract data from user request for authentication entry
 *
 * @param parserOutputList
 * 		The parser output list
 * @param root
 * 		The root to consider
 * @param type
 * 		The authentication entry type
 * @param method
 * 		The authentication method
 * @param hostname
 * 		The authentication entry hostname
 * @param user
 * 		The authentication entry user
 * @param privateData
 * 		The authentication entry private data
 * @param isPostRequest
 * 		Is it an extraction for a POST request? (If yes, all the fields must be set)
 *
 * @return if operation succeeded
 */
NBOOL NDeviceCommon_Service_Authentication_ExtractUserDataFromParserOutputList( const NParserOutputList *parserOutputList,
	const char *root,
	__OUTPUT NU32 *port,
	__OUTPUT NAuthenticationEntryType *type,
	__OUTPUT NAuthenticationMethod *method,
	__OUTPUT __ALLOC char **hostname,
	__OUTPUT __ALLOC char **user,
	__OUTPUT __ALLOC char **privateData,
	NBOOL isPostRequest )
{
	// Parser output
	const NParserOutput *parserOutput;

	// Key
	char key[ 256 ];

	// Final root
	char finalRoot[ 256 ];

	// Build root
	memset( finalRoot,
		0,
		256 );
	snprintf( finalRoot,
		256,
		"%s%s",
		root != NULL ?
			root
			: "",
		root != NULL && strlen( root ) > 0 ?
			"."
			: "" );

	// Get port
	snprintf( key,
		256,
		"%s%s",
		finalRoot,
		NDeviceCommon_Service_Authentication_NAuthenticationEntryServiceType_GetName( NAUTHENTICATION_ENTRY_SERVICE_TYPE_PORT ) );
	if( ( !( parserOutput = NParser_Output_NParserOutputList_GetFirstOutput( parserOutputList,
		key,
		NTRUE,
		NTRUE ) )
		|| NParser_Output_NParserOutput_GetType( parserOutput ) != NPARSER_OUTPUT_TYPE_INTEGER
		|| ( *port = *(NU32*)NParser_Output_NParserOutput_GetValue( parserOutput ) ) <= 0 )
		&& isPostRequest )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Quit
		return NFALSE;
	}

	// Get authentication entry type
	snprintf( key,
		256,
		"%s%s",
		finalRoot,
		NDeviceCommon_Service_Authentication_NAuthenticationEntryServiceType_GetName( NAUTHENTICATION_ENTRY_SERVICE_TYPE_TYPE ) );
	if( ( !( parserOutput = NParser_Output_NParserOutputList_GetFirstOutput( parserOutputList,
		key,
		NTRUE,
		NTRUE ) )
		|| NParser_Output_NParserOutput_GetType( parserOutput ) != NPARSER_OUTPUT_TYPE_STRING
		|| ( *type = NGhostClient_Authentication_NAuthenticationEntryType_FindType( NParser_Output_NParserOutput_GetValue( parserOutput ) ) ) >= NAUTHENTICATION_ENTRY_TYPES )
		&& isPostRequest )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Quit
		return NFALSE;
	}

	// Get authentication method
	snprintf( key,
		256,
		"%s%s",
		finalRoot,
		NDeviceCommon_Service_Authentication_NAuthenticationEntryServiceType_GetName( NAUTHENTICATION_ENTRY_SERVICE_TYPE_METHOD ) );
	if( ( !( parserOutput = NParser_Output_NParserOutputList_GetFirstOutput( parserOutputList,
		key,
		NTRUE,
		NTRUE ) )
		|| NParser_Output_NParserOutput_GetType( parserOutput ) != NPARSER_OUTPUT_TYPE_STRING
		|| ( *method = NDeviceCommon_Authentication_NAuthenticationMethod_FindAuthentication( NParser_Output_NParserOutput_GetValue( parserOutput ) ) ) >= NAUTHENTICATION_METHODS )
		&& isPostRequest )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Quit
		return NFALSE;
	}

	// Get hostname
	snprintf( key,
		256,
		"%s%s",
		finalRoot,
		NDeviceCommon_Service_Authentication_NAuthenticationEntryServiceType_GetName( NAUTHENTICATION_ENTRY_SERVICE_TYPE_HOSTNAME ) );
	if( ( !( parserOutput = NParser_Output_NParserOutputList_GetFirstOutput( parserOutputList,
		key,
		NTRUE,
		NTRUE ) )
		|| NParser_Output_NParserOutput_GetType( parserOutput ) != NPARSER_OUTPUT_TYPE_STRING
		|| !( *hostname = NParser_Output_NParserOutput_GetValueCopy( parserOutput ) ) )
		&& isPostRequest )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Quit
		return NFALSE;
	}

	// Get user
	snprintf( key,
		256,
		"%s%s",
		finalRoot,
		NDeviceCommon_Service_Authentication_NAuthenticationEntryServiceType_GetName( NAUTHENTICATION_ENTRY_SERVICE_TYPE_USER ) );
	if( ( !( parserOutput = NParser_Output_NParserOutputList_GetFirstOutput( parserOutputList,
		key,
		NTRUE,
		NTRUE ) )
		|| NParser_Output_NParserOutput_GetType( parserOutput ) != NPARSER_OUTPUT_TYPE_STRING
		|| !( *user = NParser_Output_NParserOutput_GetValueCopy( parserOutput ) ) )
		&& isPostRequest )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Free
		NFREE( *hostname );

		// Quit
		return NFALSE;
	}

	// Get private data
	snprintf( key,
		256,
		"%s%s",
		finalRoot,
		NDeviceCommon_Service_Authentication_NAuthenticationEntryServiceType_GetName( NAUTHENTICATION_ENTRY_SERVICE_TYPE_PRIVATE_DATA ) );
	if( ( ( parserOutput = NParser_Output_NParserOutputList_GetFirstOutput( parserOutputList,
		key,
		NTRUE,
		NTRUE ) ) != NULL
		&& NParser_Output_NParserOutput_GetType( parserOutput ) == NPARSER_OUTPUT_TYPE_STRING
		&& !( *privateData = NParser_Output_NParserOutput_GetValueCopy( parserOutput ) ) )
		&& isPostRequest )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Free
		NFREE( *user );
		NFREE( *hostname );

		// Quit
		return NFALSE;
	}

	// OK
	return NTRUE;
}

/**
 * Extract and add data
 *
 */
NBOOL NDeviceCommon_Service_Authentication_ExtractAddData( const NParserOutputList *parserOutputList,
	const char *root,
	NAuthenticationManager *authenticationManager )
{
	// Authentication entry
	NAuthenticationEntry *authenticationEntry = NULL;

	// Hostname
	char *hostname;

	// Port
	NU32 port;

	// User
	char *user;

	// Type
	NAuthenticationEntryType type;

	// Method
	NAuthenticationMethod method;

	// Private data
	char *privateData = NULL;

	// Extract user data
	if( !NDeviceCommon_Service_Authentication_ExtractUserDataFromParserOutputList( parserOutputList,
		root,
		&port,
		&type,
		&method,
		&hostname,
		&user,
		&privateData,
		NTRUE ) )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Quit
		return NFALSE;
	}

	// Check method
	if( method >= NAUTHENTICATION_METHODS )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Free
		NFREE( hostname );
		NFREE( privateData );
		NFREE( user );

		// Quit
		return NFALSE;
	}

	// Build new authentication entry
	switch( method )
	{
		case NAUTHENTICATION_METHOD_NO_AUTHENTICATION:
		case NAUTHENTICATION_METHOD_TOKEN:
			authenticationEntry = NDeviceCommon_Authentication_NAuthenticationEntry_Build( hostname,
				port,
				user,
				NULL,
				type,
				NAUTHENTICATION_METHOD_NO_AUTHENTICATION,
				NFALSE );
			break;

		default:
			// Check private data
			if( privateData != NULL )
				// Build authentication entry
				authenticationEntry = NDeviceCommon_Authentication_NAuthenticationEntry_Build( hostname,
					port,
					user,
					privateData,
					type,
					method,
					NFALSE );
			break;
	}

	// Free
	NFREE( hostname );
	NFREE( privateData );
	NFREE( user );

	// Check authentication entry
	if( !authenticationEntry )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NFALSE;
	}

	// Add
	return NDeviceCommon_Authentication_NAuthenticationManager_AddAuthentication( authenticationManager,
		authenticationEntry );
}

/**
 * Process authentication REST POST request
 *
 * @param authenticationManager
 * 		The authentication manager
 * @param clientData
 * 		The client data
 * @param clientDataLength
 * 		The client data length
 *
 * @return if operation succeeded
 */
NBOOL NDeviceCommon_Service_Authentication_ProcessRESTPOSTRequest( __MUSTBEPROTECTED NAuthenticationManager *authenticationManager,
	const char *clientData,
	NU32 clientDataLength )
{
	// Parser output list
	NParserOutputList *parserOutputList;

	// Iterator
	NU32 i = 0;

	// Parser output
	const NParserOutput *parserOutput;

	// Buffer
	char buffer[ 256 ];

	// Parse json
	if( !( parserOutputList = NJson_Engine_Parser_Parse( clientData,
		clientDataLength ) ) )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Quit
		return NFALSE;
	}

	// Check if count key present
	if( ( parserOutput = NParser_Output_NParserOutputList_GetFirstOutput( parserOutputList,
		NDeviceCommon_Service_Authentication_NAuthenticationServiceType_GetName( NAUTHENTICATION_SERVICE_TYPE_COUNT ),
		NTRUE,
		NTRUE ) ) != NULL )
	{
		// Analyze type
		if( NParser_Output_NParserOutput_GetType( parserOutput ) != NPARSER_OUTPUT_TYPE_INTEGER )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_SYNTAX );

			// Destroy
			NParser_Output_NParserOutputList_Destroy( &parserOutputList );

			// Quit
			return NFALSE;
		}

		// Iterate
		for( i = 0; i < *( (NU32*)NParser_Output_NParserOutput_GetValue( parserOutput ) ); i++ )
		{
			// Build root
			snprintf( buffer,
				256,
				"%d",
				i );

			// Extract data
			NDeviceCommon_Service_Authentication_ExtractAddData( parserOutputList,
				buffer,
				authenticationManager );
		}
	}
	else
		// Extract data
		if( !NDeviceCommon_Service_Authentication_ExtractAddData( parserOutputList,
			NULL,
			authenticationManager ) )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_SYNTAX );

			// Destroy
			NParser_Output_NParserOutputList_Destroy( &parserOutputList );

			// Quit
			return NFALSE;
		}

	// Destroy parser output list
	NParser_Output_NParserOutputList_Destroy( &parserOutputList );

	// Save
	return NDeviceCommon_Authentication_NAuthenticationManager_Save( authenticationManager );
}

/**
 * Process authentication REST PUT request
 *
 * @param authenticationManager
 * 		The authentication manager
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 * @param clientData
 * 		The client data
 * @param clientDataLength
 * 		The client data length
 *
 * @return if operation succeeded
 */
NBOOL NDeviceCommon_Service_Authentication_ProcessRESTPUTRequest( __MUSTBEPROTECTED NAuthenticationManager *authenticationManager,
	const char *requestedElement,
	NU32 *cursor,
	const char *clientData,
	NU32 clientDataLength )
{
	// Authentication entry index
	NU32 authenticationEntryIndex;

	// Authentication entry
	NAuthenticationEntry *authenticationEntry;

	// Parse service type
	switch( NDeviceCommon_Service_Authentication_NAuthenticationServiceType_FindServiceType( authenticationManager,
		requestedElement,
		cursor,
		&authenticationEntryIndex ) )
	{
		case NAUTHENTICATION_SERVICE_TYPE_AUTHENTICATION_ENTRY:
			// Get entry
			if( !( authenticationEntry = (NAuthenticationEntry*)NDeviceCommon_Authentication_NAuthenticationManager_GetAuthenticationByIndex( authenticationManager,
				authenticationEntryIndex ) ) )
			{
				// Notify
				NOTIFIER_ERREUR( NERREUR_UNKNOWN );

				// Quit
				return NFALSE;
			}

			// Update entry
			if( !NDeviceCommon_Authentication_NAuthenticationEntry_Update( authenticationEntry,
				clientData,
				clientDataLength ) )
			{
				// Notify
				NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

				// Quit
				return NFALSE;
			}
			break;

		default:
			// Notify
			NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

			// Quit
			return NFALSE;
	}

	// Save
	NDeviceCommon_Authentication_NAuthenticationManager_Save( authenticationManager );

	// OK
	return NTRUE;
}

/**
 * Build parser output list for service (private)
 *
 * @param authenticationManager
 * 		The authentication manager
 * @param serviceType
 * 		The service type
 * @param parserOutputList
 * 		The parser output list
 * @param jsonKeyRoot
 * 		The key root
 *
 * @return if operation succeeded
 */
__PRIVATE NBOOL NDeviceCommon_Service_Authentication_BuildParserOutputListForAuthenticationService( __MUSTBEPROTECTED NAuthenticationManager *authenticationManager,
	NAuthenticationServiceType serviceType,
	__OUTPUT NParserOutputList *parserOutputList,
	const char *jsonKeyRoot,
	NU32 authenticationIndex )
{
	// Key
	char key[ 512 ];

	// Buffer
	char buffer[ 512 ];

	// Corrected hostname
	char *correctedHostname;

	// Authentication entry
	const NAuthenticationEntry *authenticationEntry;

	// Entry service type
	NAuthenticationEntryServiceType entryServiceType;

	// Build key root
	snprintf( key,
		512,
		"%s%s",
		jsonKeyRoot,
		strlen( jsonKeyRoot ) > 0 ?
			"."
			: "" );

	// Process
	switch( serviceType )
	{
		case NAUTHENTICATION_SERVICE_TYPE_ROOT:
			break;

		case NAUTHENTICATION_SERVICE_TYPE_COUNT:
			// Add count key
			strcat( key,
				NDeviceCommon_Service_Authentication_NAuthenticationServiceType_GetName( serviceType ) );

			// Add value
			NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
				key,
				NDeviceCommon_Authentication_NAuthenticationManager_GetAuthenticationCount( authenticationManager ) );
			break;
		case NAUTHENTICATION_SERVICE_TYPE_AUTHENTICATION_ENTRY:
			// Get authentication entry
			if( ( authenticationEntry = NDeviceCommon_Authentication_NAuthenticationManager_GetAuthenticationByIndex( authenticationManager,
				authenticationIndex ) ) != NULL )
				if( ( correctedHostname = NDeviceCommon_Authentication_NAuthenticationEntry_GenerateJsonBuilderCompatibleHostname( authenticationEntry ) ) != NULL )
				{
					// Fill buffer
					snprintf( buffer,
						512,
						NDeviceCommon_Service_Authentication_NAuthenticationServiceType_GetName( serviceType ),
						correctedHostname,
						NDeviceCommon_Authentication_NAuthenticationEntry_GetPort( authenticationEntry ) );

					// Free
					NFREE( correctedHostname );

					// Add to key
					strcat( key,
						buffer );

					// Add
					for( entryServiceType = (NAuthenticationEntryServiceType)0; entryServiceType < NAUTHENTICATION_ENTRY_SERVICE_TYPES; entryServiceType++ )
						// Add authentication entry property
						NDeviceCommon_Authentication_NAuthenticationEntry_BuildParserOutputListInternal( authenticationEntry,
							key,
							parserOutputList,
							entryServiceType );
				}

			break;

		default:
			return NFALSE;
	}

	// OK
	return NTRUE;
}

/**
 * Process authentication REST GET request
 *
 * @param authenticationManager
 * 		The authentication manager
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 * @param clientID
 * 		The client ID
 *
 * @return the http response
 */
__ALLOC NReponseHTTP *NDeviceCommon_Service_Authentication_ProcessRESTGETRequest( __MUSTBEPROTECTED NAuthenticationManager *authenticationManager,
	const char *requestedElement,
	NU32 *cursor,
	NU32 clientID )
{
	// Parser output list
	NParserOutputList *parserOutputList;

	// Authentication entry index
	NU32 authenticationEntryIndex;

	// Authentication entry
	const NAuthenticationEntry *authenticationEntry;

	// Entry service type
	NAuthenticationEntryServiceType entryServiceType;

	// Create parser output list
	if( !( parserOutputList = NParser_Output_NParserOutputList_Build( ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NULL;
	}

	// Find service type
	switch( NDeviceCommon_Service_Authentication_NAuthenticationServiceType_FindServiceType( authenticationManager,
		requestedElement,
		cursor,
		&authenticationEntryIndex ) )
	{
		case NAUTHENTICATION_SERVICE_TYPE_ROOT:
			// Add count
			NDeviceCommon_Service_Authentication_BuildParserOutputListForAuthenticationService( authenticationManager,
				NAUTHENTICATION_SERVICE_TYPE_COUNT,
				parserOutputList,
				"",
				0 );

			// Add authentication entries
			for( authenticationEntryIndex = 0; authenticationEntryIndex < NDeviceCommon_Authentication_NAuthenticationManager_GetAuthenticationCount( authenticationManager ); authenticationEntryIndex++ )
				NDeviceCommon_Service_Authentication_BuildParserOutputListForAuthenticationService( authenticationManager,
					NAUTHENTICATION_SERVICE_TYPE_AUTHENTICATION_ENTRY,
					parserOutputList,
					"",
					authenticationEntryIndex );
			break;

		case NAUTHENTICATION_SERVICE_TYPE_COUNT:
			// Add count
			NDeviceCommon_Service_Authentication_BuildParserOutputListForAuthenticationService( authenticationManager,
				NAUTHENTICATION_SERVICE_TYPE_COUNT,
				parserOutputList,
				"",
				0 );
			break;

		case NAUTHENTICATION_SERVICE_TYPE_AUTHENTICATION_ENTRY:
			// Get authentication entry
			if( ( authenticationEntry = NDeviceCommon_Authentication_NAuthenticationManager_GetAuthenticationByIndex( authenticationManager,
				authenticationEntryIndex ) ) != NULL )
				// Add
				switch( entryServiceType = NDeviceCommon_Service_Authentication_NAuthenticationEntryServiceType_FindServiceType( requestedElement,
					cursor ) )
				{
					case NAUTHENTICATION_ENTRY_SERVICE_TYPE_ROOT:
						for( entryServiceType = (NAuthenticationEntryServiceType)0;
							entryServiceType < NAUTHENTICATION_ENTRY_SERVICE_TYPES; entryServiceType++ )
							// Add authentication entry property
							NDeviceCommon_Authentication_NAuthenticationEntry_BuildParserOutputListInternal( authenticationEntry,
								"",
								parserOutputList,
								entryServiceType );
						break;

					default:
						// Add
						NDeviceCommon_Authentication_NAuthenticationEntry_BuildParserOutputListInternal( authenticationEntry,
							"",
							parserOutputList,
							entryServiceType );
						break;
				}
			break;

		default:
			// Destroy
			NParser_Output_NParserOutputList_Destroy( &parserOutputList );

			// Quit
			return NULL;
	}

	// OK
	return NDeviceCommon_Service_GenerateJsonHTTPResponseWithParserOutputList( NHTTP_CODE_200_OK,
		parserOutputList,
		clientID );
}


/**
 * Process authentication REST DELETE request
 *
 * @param authenticationManager
 * 		The authentication manager
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 * @param clientID
 * 		The client ID
 *
 * @return if operation succeeded
 */
NBOOL NDeviceCommon_Service_Authentication_ProcessRESTDELETERequest( __MUSTBEPROTECTED NAuthenticationManager *authenticationManager,
	const char *requestedElement,
	NU32 *cursor )
{
	// Authentication entry index
	NU32 authenticationEntryIndex;

	// Result
	__OUTPUT NBOOL result;

	// Find service type
	switch( NDeviceCommon_Service_Authentication_NAuthenticationServiceType_FindServiceType( authenticationManager,
		requestedElement,
		cursor,
		&authenticationEntryIndex ) )
	{
		case NAUTHENTICATION_SERVICE_TYPE_AUTHENTICATION_ENTRY:
			// Remove
			result = NDeviceCommon_Authentication_NAuthenticationManager_RemoveAuthenticationByIndex( authenticationManager,
				authenticationEntryIndex );

			// Save
			NDeviceCommon_Authentication_NAuthenticationManager_Save( authenticationManager );

			// OK?
			return result;

		default:
			return NFALSE;
	}
}

