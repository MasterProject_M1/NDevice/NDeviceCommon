#include "../../include/NDeviceCommon.h"

// ------------------------------------------------------------
// struct NDeviceCommon::Authentication::NAuthenticationManager
// ------------------------------------------------------------

/**
 * Load authentication details (private)
 *
 * @param this
 * 		This instance
 *
 * @return if operation succeeded
 */
__PRIVATE NBOOL NDeviceCommon_Authentication_NAuthenticationManager_Load( NAuthenticationManager *this )
{
	// File
	NFichierTexte *file;

	// Buffer
	char buffer[ 128 ];

	// Line
	char *line;

	// Authentication entry
	NAuthenticationEntry *entry;

	// Build authentication file path
	snprintf( buffer,
		128,
		"%s.auth",
		NDeviceCommon_Type_NDeviceType_GetName( this->m_deviceType ) );

	// Open file
	if( !( file = NLib_Fichier_NFichierTexte_ConstruireLecture( buffer ) ) )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_FILE_CANNOT_BE_OPENED );

		// Quit
		return NFALSE;
	}

	// Read line
	while( ( line = NLib_Fichier_NFichierTexte_LireProchaineLigneSansCommentaire( file,
		'#',
		NFALSE ) ) )
	{
		// Parse line
		if( ( entry = NDeviceCommon_Authentication_NAuthenticationEntry_Build2( line ) ) )
			// Add
			NDeviceCommon_Authentication_NAuthenticationManager_AddAuthentication( this,
				entry );

		// Free
		NFREE( line );
	}

	// Close file
	NLib_Fichier_NFichierTexte_Detruire( &file );

	// OK
	return NTRUE;
}

/**
 * Remove authentication by index
 *
 * @param this
 * 		This instance
 * @param index
 * 		The index
 * @param isForce
 * 		Do we force remove?
 *
 * @return if the operation succeeded
 */
__PRIVATE __MUSTBEPROTECTED NBOOL NDeviceCommon_Authentication_NAuthenticationManager_RemoveAuthenticationByIndex2( NAuthenticationManager *this,
	NU32 index,
	NBOOL isForce )
{
	// Entry
	const NAuthenticationEntry *entry;

	// Get entry
	if( !( entry = NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_authentication,
		index ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quit
		return NFALSE;
	}

	// Check read only flag
	if( !isForce
		&& NDeviceCommon_Authentication_NAuthenticationEntry_IsReadOnly( entry ) )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_OBJECT_STATE );

		// Quit
		return NFALSE;
	}

	// Remove
	return NLib_Memoire_NListe_SupprimerDepuisIndex( this->m_authentication,
		index );
}

/**
 * Add own authentication details
 *
 * @param this
 * 		This instance
 * @param serviceUsername
 *		The service username
 * @param servicePassword
 * 		The service password
 * @param servicePort
 * 		The service port
 * @param contactingIP
 * 		The contacting
 *
 * @return if the operation succeeded
 */
__PRIVATE NBOOL NDeviceCommon_Authentication_NAuthenticationManager_AddOwnAuthenticationDetail( NAuthenticationManager *this,
	const char *serviceUsername,
	const char *servicePassword,
	NU32 servicePort,
	const char *contactingIP )
{
	// Entry index
	NU32 index;

	// Authentication entry
	NAuthenticationEntry *authenticationEntry;

	// Look for existing authentication entry index
	if( ( index = NDeviceCommon_Authentication_NAuthenticationManager_FindAuthenticationEntryIndexByIP( this,
		"127.0.0.1",
		servicePort ) ) != NERREUR )
		// Remove entry
		NDeviceCommon_Authentication_NAuthenticationManager_RemoveAuthenticationByIndex2( this,
			index,
			NTRUE );

	// Build authentication entry
	if( !( authenticationEntry = NDeviceCommon_Authentication_NAuthenticationEntry_Build( "127.0.0.1",
		servicePort,
		serviceUsername,
		servicePassword,
		NAUTHENTICATION_ENTRY_TYPE_HTTP_BASIC,
		NAUTHENTICATION_METHOD_PASSWORD,
		NFALSE ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NFALSE;
	}

	// Add entry
	NDeviceCommon_Authentication_NAuthenticationManager_AddAuthentication( this,
		authenticationEntry );

	// Build authentication entry
	if( !( authenticationEntry = NDeviceCommon_Authentication_NAuthenticationEntry_Build( contactingIP,
		servicePort,
		serviceUsername,
		servicePassword,
		NAUTHENTICATION_ENTRY_TYPE_HTTP_BASIC,
		NAUTHENTICATION_METHOD_PASSWORD,
		NFALSE ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NFALSE;
	}

	// Add entry
	NDeviceCommon_Authentication_NAuthenticationManager_AddAuthentication( this,
		authenticationEntry );
}

/**
 * Build authentication manager
 *
 * @param deviceType
 * 		The device type
 * @param isPersistent
 * 		Is persistent?
 * @param serviceUsername
 * 		The service configured username
 * @param servicePassword
 * 		The service configured password
 * @param servicePort
 * 		The service listening port
 * @param contactingIP
 * 		The contacting IP
 *
 * @return the authentication manager instance
 */
__ALLOC NAuthenticationManager *NDeviceCommon_Authentication_NAuthenticationManager_Build( NDeviceType deviceType,
	NBOOL isPersistent,
	const char *serviceUsername,
	const char *servicePassword,
	NU32 servicePort,
	const char *contactingIP )
{
	// Output
	__OUTPUT NAuthenticationManager *out;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NAuthenticationManager ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Create list
	if( !( out->m_authentication = NLib_Memoire_NListe_Construire( (void ( ___cdecl* )( void* ))NDeviceCommon_Authentication_NAuthenticationEntry_Destroy ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// Save
	out->m_deviceType = deviceType;
	out->m_isPersistent = isPersistent;

	// Load authentication
	NDeviceCommon_Authentication_NAuthenticationManager_Load( out );

	// Add own authentication details
	NDeviceCommon_Authentication_NAuthenticationManager_AddOwnAuthenticationDetail( out,
		serviceUsername,
		servicePassword,
		servicePort,
		contactingIP );

	// Save authentication
	NDeviceCommon_Authentication_NAuthenticationManager_Save( out );

	// OK
	return out;
}

/**
 * Destroy authentication manager
 *
 * @param this
 * 		This instance
 */
void NDeviceCommon_Authentication_NAuthenticationManager_Destroy( NAuthenticationManager **this )
{
	// Destroy list
	NLib_Memoire_NListe_Detruire( &(*this)->m_authentication );

	// Free
	NFREE( (*this) );
}

/**
 * Activate protection
 *
 * @param this
 * 		This instance
 */
__WILLLOCK void NDeviceCommon_Authentication_NAuthenticationManager_ActivateProtection( NAuthenticationManager *this )
{
	NLib_Memoire_NListe_ActiverProtection( this->m_authentication );
}

/**
 * Deactivate protection
 *
 * @param this
 * 		This instance
 */
__WILLUNLOCK void NDeviceCommon_Authentication_NAuthenticationManager_DeactivateProtection( NAuthenticationManager *this )
{
	NLib_Memoire_NListe_DesactiverProtection( this->m_authentication );
}

/**
 * Add authentication
 *
 * @param this
 * 		This instance
 * @param authenticationEntry
 * 		The authentication entry to add
 *
 * @return if operation succeeded
 */
__MUSTBEPROTECTED NBOOL NDeviceCommon_Authentication_NAuthenticationManager_AddAuthentication( NAuthenticationManager *this,
	__WILLBEOWNED NAuthenticationEntry *authenticationEntry )
{
	// Check if already present
	if( NDeviceCommon_Authentication_NAuthenticationManager_FindAuthenticationForHostnameAndPort( this,
		NDeviceCommon_Authentication_NAuthenticationEntry_GetHostname( authenticationEntry ),
		NDeviceCommon_Authentication_NAuthenticationEntry_GetPort( authenticationEntry ) ) != NULL )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_FILE_ALREADY_EXISTS );

		// Destroy
		NDeviceCommon_Authentication_NAuthenticationEntry_Destroy( &authenticationEntry );

		// Quit
		return NFALSE;
	}

	// Add
	return NLib_Memoire_NListe_Ajouter( this->m_authentication,
		authenticationEntry );
}

/**
 * Find authentication index by ip and port
 *
 * @param this
 * 		This instance
 * @param ip
 * 		The ip
 * @param port
 * 		The port
 *
 * @return the authentication entry index or NERREUR
 */
__MUSTBEPROTECTED NU32 NDeviceCommon_Authentication_NAuthenticationManager_FindAuthenticationEntryIndexByIP( const NAuthenticationManager *this,
	const char *ip,
	NU32 port )
{
	// Iterator
	__OUTPUT NU32 i = 0;

	// Authentication entry
	const NAuthenticationEntry *entry;

	// Look for
	for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_authentication ); i++ )
		// Get entry
		if( ( entry = NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_authentication,
			i ) ) != NULL )
			// IP and port corrects?
			if( NLib_Chaine_Comparer( NDeviceCommon_Authentication_NAuthenticationEntry_GetIP( entry ),
				ip,
				NTRUE,
				0 )
				&& NDeviceCommon_Authentication_NAuthenticationEntry_GetPort( entry ) == port )
				// Found it
				return i;

	// Not found
	return NERREUR;
}

/**
 * Find authentication index by hostname and port
 *
 * @param this
 * 		This instance
 * @param ip
 * 		The ip
 * @param port
 * 		The port
 *
 * @return the authentication entry index or NERREUR
 */
__MUSTBEPROTECTED NU32 NDeviceCommon_Authentication_NAuthenticationManager_FindAuthenticationEntryIndexByHostname( const NAuthenticationManager *this,
	const char *hostname,
	NU32 port )
{
	// Iterator
	__OUTPUT NU32 i = 0;

	// Authentication entry
	const NAuthenticationEntry *entry;

	// Look for
	for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_authentication ); i++ )
		// Get entry
		if( ( entry = NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_authentication,
			i ) ) != NULL )
			// IP and port corrects?
			if( NLib_Chaine_Comparer( NDeviceCommon_Authentication_NAuthenticationEntry_GetHostname( entry ),
				hostname,
				NTRUE,
				0 )
				&& NDeviceCommon_Authentication_NAuthenticationEntry_GetPort( entry ) == port )
				// Found it
				return i;

	// Not found
	return NERREUR;
}

/**
 * Find authentication by ip and port
 *
 * @param this
 * 		This instance
 * @param ip
 * 		The authentication ip to find
 * @param port
 * 		The authentication port to find
 *
 * @return the authentication entry or NULL
 */
__MUSTBEPROTECTED const NAuthenticationEntry *NDeviceCommon_Authentication_NAuthenticationManager_FindAuthenticationForIPAndPort( const NAuthenticationManager *this,
	const char *ip,
	NU32 port )
{
	// Index
	NU32 index;

	// Find entry index
	if( ( index = NDeviceCommon_Authentication_NAuthenticationManager_FindAuthenticationEntryIndexByIP( this,
		ip,
		port ) ) == NERREUR )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_PARAMETER_ERROR );

		// Quit
		return NULL;
	}

	// OK
	return NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_authentication,
		index );
}

/**
 * Find authentication by hostname and port
 *
 * @param this
 * 		This instance
 * @param hostname
 * 		The authentication ip to find
 * @param port
 * 		The authentication port to find
 *
 * @return the authentication entry or NULL
 */
__MUSTBEPROTECTED const NAuthenticationEntry *NDeviceCommon_Authentication_NAuthenticationManager_FindAuthenticationForHostnameAndPort( const NAuthenticationManager *this,
	const char *hostname,
	NU32 port )
{
	// Index
	NU32 index;

	// Find entry index
	if( ( index = NDeviceCommon_Authentication_NAuthenticationManager_FindAuthenticationEntryIndexByHostname( this,
		hostname,
		port ) ) == NERREUR )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_PARAMETER_ERROR );

		// Quit
		return NULL;
	}

	// OK
	return NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_authentication,
		index );
}

/**
 * Get authentication count
 *
 * @param this
 * 		This instance
 *
 * @return the authentication entry count
 */
__MUSTBEPROTECTED NU32 NDeviceCommon_Authentication_NAuthenticationManager_GetAuthenticationCount( const NAuthenticationManager *this )
{
	return NLib_Memoire_NListe_ObtenirNombre( this->m_authentication );
}

/**
 * Get authentication by index
 *
 * @param this
 * 		This instance
 * @param index
 * 		The index
 *
 * @return the authentication at %index%
 */
__MUSTBEPROTECTED const NAuthenticationEntry *NDeviceCommon_Authentication_NAuthenticationManager_GetAuthenticationByIndex( const NAuthenticationManager *this,
	NU32 index )
{
	return NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_authentication,
		index );
}

/**
 * Remove authentication by ip and port
 *
 * @param this
 * 		This instance
 * @param ip
 * 		The authentication ip
 * @param port
 * 		The port
 *
 * @return if operation succeeded
 */
__MUSTBEPROTECTED NBOOL NDeviceCommon_Authentication_NAuthenticationManager_RemoveAuthenticationByIPAndPort( NAuthenticationManager *this,
	const char *ip,
	NU32 port )
{
	// Index
	NU32 index;

	// Find index
	if( ( index = NDeviceCommon_Authentication_NAuthenticationManager_FindAuthenticationEntryIndexByIP( this,
		ip,
		port ) ) == NERREUR )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quit
		return NFALSE;
	}

	// Remove
	return NDeviceCommon_Authentication_NAuthenticationManager_RemoveAuthenticationByIndex( this,
		index );
}

/**
 * Remove authentication by hostname and port
 *
 * @param this
 * 		This instance
 * @param hostname
 * 		The authentication hostname
 * @param port
 * 		The port
 *
 * @return if operation succeeded
 */
__MUSTBEPROTECTED NBOOL NDeviceCommon_Authentication_NAuthenticationManager_RemoveAuthenticationByHostnameAndPort( NAuthenticationManager *this,
	const char *hostname,
	NU32 port )
{
	// Index
	NU32 index;

	// Find index
	if( ( index = NDeviceCommon_Authentication_NAuthenticationManager_FindAuthenticationEntryIndexByHostname( this,
		hostname,
		port ) ) == NERREUR )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quit
		return NFALSE;
	}

	// Remove
	return NDeviceCommon_Authentication_NAuthenticationManager_RemoveAuthenticationByIndex( this,
		index );
}

/**
 * Remove authentication by index
 *
 * @param this
 * 		This instance
 * @param index
 * 		The index
 *
 * @return if operation succeeded
 */
__MUSTBEPROTECTED NBOOL NDeviceCommon_Authentication_NAuthenticationManager_RemoveAuthenticationByIndex( NAuthenticationManager *this,
	NU32 index )
{
	return NDeviceCommon_Authentication_NAuthenticationManager_RemoveAuthenticationByIndex2( this,
		index,
		NFALSE );
}

/**
 * Process REST request
 *
 * @param this
 * 		This instance
 * @param requestedElement
 * 		The requested element
 * @param requestType
 * 		The http request type
 * @param data
 * 		The request data
 * @param dataLength
 * 		The data length
 * @param clientID
 * 		The client ID
 *
 * @return the http response
 */
__ALLOC __WILLLOCK __WILLUNLOCK NReponseHTTP *NDeviceCommon_Authentication_NAuthenticationManager_ProcessRESTRequest( NAuthenticationManager *this,
	const char *requestedElement,
	NTypeRequeteHTTP requestType,
	const char *data,
	NU32 dataLength,
	NU32 clientID )
{
	// HTTP response
	NReponseHTTP *response = NULL;

	// Cursor
	NU32 cursor = 0;

	// Lock
	NLib_Memoire_NListe_ActiverProtection( this->m_authentication );

	// Process
	switch( requestType )
	{
		case NTYPE_REQUETE_HTTP_PUT:
			if( NDeviceCommon_Service_Authentication_ProcessRESTPUTRequest( this,
				requestedElement,
				&cursor,
				data,
				dataLength ) )
				// Create response
				response = NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NDEVICE_COMMON_TYPE_GHOST_OK_MESSAGE,
					NHTTP_CODE_200_OK,
					clientID );
			break;
		case NTYPE_REQUETE_HTTP_POST:
			// Check requested element
			if( strlen( requestedElement ) > 0 )
			{
				// Notify
				NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

				// Unlock
				NLib_Memoire_NListe_DesactiverProtection( this->m_authentication );

				// Quit
				return NULL;
			}

			// Process
			if( NDeviceCommon_Service_Authentication_ProcessRESTPOSTRequest( this,
				data,
				dataLength ) )
				// Create response
				response = NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NDEVICE_COMMON_TYPE_GHOST_OK_MESSAGE,
					NHTTP_CODE_200_OK,
					clientID );
			break;
		case NTYPE_REQUETE_HTTP_GET:
			// Process
			response = NDeviceCommon_Service_Authentication_ProcessRESTGETRequest( this,
				requestedElement,
				&cursor,
				clientID );
			break;
		case NTYPE_REQUETE_HTTP_DELETE:
			// Process
			if( NDeviceCommon_Service_Authentication_ProcessRESTDELETERequest( this,
				requestedElement,
				&cursor ) )
				// Create response
				response = NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NDEVICE_COMMON_TYPE_GHOST_OK_MESSAGE,
					NHTTP_CODE_200_OK,
					clientID );
			break;

		default:
			// Unlock
			NLib_Memoire_NListe_DesactiverProtection( this->m_authentication );

			// Quit
			return NULL;
	}

	// Unlock
	NLib_Memoire_NListe_DesactiverProtection( this->m_authentication );

	// OK
	return response;
}

/**
 * Save authentication details
 *
 * @param this
 * 		This instance
 *
 * @return if operation succeeded
 */
__MUSTBEPROTECTED NBOOL NDeviceCommon_Authentication_NAuthenticationManager_Save( const NAuthenticationManager *this )
{
	// File
	NFichierTexte *file;

	// File path
	char filePath[ 128 ];

	// Iterator
	NU32 i;

	// Entry
	const NAuthenticationEntry *entry;

	// Is it persistent?
	if( !this->m_isPersistent )
		// Quit without error
		return NTRUE;

	// Create file path
	snprintf( filePath,
		128,
		"%s.auth",
		NDeviceCommon_Type_NDeviceType_GetName( this->m_deviceType ) );

	// Open file
	if( !( file = NLib_Fichier_NFichierTexte_ConstruireEcriture( filePath,
		NTRUE ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_FILE_CANNOT_BE_OVERRIDE );

		// Quit
		return NFALSE;
	}

	// Write entries
	for( i = 0; i < NLib_Memoire_NListe_ObtenirNombre( this->m_authentication ); i++ )
		// Get entry
		if( ( entry = NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_authentication,
			i ) ) != NULL )
			// Save entry
			NDeviceCommon_Authentication_NAuthenticationEntry_Save( entry,
				file );

	// Close file
	NLib_Fichier_NFichierTexte_Detruire( &file );

	// OK
	return NTRUE;
}

/**
 * Send all known authentication details
 *
 * @param this
 * 		This instance
 * @param httpClient
 * 		The http client where to send
 * @param authorizationToken
 * 		The authorization token
 *
 * @return if operation succeeded
 */
__WILLLOCK __WILLUNLOCK NBOOL NDeviceCommon_Authentication_NAuthenticationManager_SendAllAuthentication( const NAuthenticationManager *this,
	NClientHTTP *httpClient,
	const char *authorizationToken )
{
	// Key
	char key[ 128 ];

	// Iterator
	NU32 i = 0;

	// Buffer
	char buffer[ 256 ];

	// Request
	NRequeteHTTP *request;

	// Json
	char *json;

	// Base 64 token
	char *base64Token;

	// Parser output list
	NParserOutputList *parserOutputList;

	// Authentication entry
	const NAuthenticationEntry *authenticationEntry;

	// Build parser output list
	if( !( parserOutputList = NParser_Output_NParserOutputList_Build( ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NFALSE;
	}

	// Lock authentication manager
	NLib_Memoire_NListe_ActiverProtection( this->m_authentication );

	// Add count
	NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
		NDeviceCommon_Service_Authentication_NAuthenticationServiceType_GetName( NAUTHENTICATION_SERVICE_TYPE_COUNT ),
		NLib_Memoire_NListe_ObtenirNombre( this->m_authentication ) );

	// Iterate authentication entries
	for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_authentication ); i++ )
		// Get entry
		if( ( authenticationEntry = NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_authentication,
			i ) ) != NULL )
			// Check ip
			if( !NLib_Chaine_Comparer( NDeviceCommon_Authentication_NAuthenticationEntry_GetHostname( authenticationEntry ),
				"127.0.0.1",
				NTRUE,
				0 ) )
			{
				// Build key
				snprintf( key,
					128,
					"%d",
					i );

				// Add properties
				NDeviceCommon_Authentication_NAuthenticationEntry_BuildParserOutputList( authenticationEntry,
					key,
					parserOutputList );
			}

	// Unlock authentication manager
	NLib_Memoire_NListe_DesactiverProtection( this->m_authentication );

	// Build json
	if( !( json = NJson_Engine_Builder_Build( parserOutputList ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Destroy parser output list
		NParser_Output_NParserOutputList_Destroy( &parserOutputList );

		// Quit
		return NFALSE;
	}

	// Destroy parser output list
	NParser_Output_NParserOutputList_Destroy( &parserOutputList );

	// Build packet
	if( !( request = NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_Construire2( NTYPE_REQUETE_HTTP_POST,
		NDEVICE_COMMON_TYPE_GHOST_AUTHENTICATION ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( json );

		// Quit
		return NFALSE;
	}

	// Bufferize token
	snprintf( buffer,
		256,
		"%s:",
		authorizationToken );

	// Add authorization header
	if( ( base64Token = NLib_Chaine_ConvertirBase64( buffer,
		(NU32)strlen( buffer ) ) ) != NULL )
	{
		// Build buffer
		snprintf( buffer,
			256,
			"Basic %s",
			base64Token );

		// Add header
		NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_AjouterAttribut( request,
			NMOT_CLEF_REQUETE_HTTP_KEYWORD_AUTHORIZATION,
			buffer );

		// Free
		NFREE( base64Token );
	}

	// Set data
	NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_AjouterData( request,
		json,
		(NU32)strlen( json ) );

	// Free json
	NFREE( json );

	// Send packet
	return NHTTP_Client_NClientHTTP_EnvoyerRequete( httpClient,
		request,
		NDEVICE_COMMON_AUTHENTICATION_BROADCAST_CONNECTION_TIMEOUT );
}
