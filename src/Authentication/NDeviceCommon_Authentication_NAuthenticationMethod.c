#define NDEVICECOMMON_AUTHENTICATION_NAUTHENTICATIONMETHOD_INTERNE
#include "../../include/NDeviceCommon.h"

// ---------------------------------------------------------
// enum NDeviceCommon::Authentication::NAuthenticationMethod
// ---------------------------------------------------------

/**
 * Get authentication method name
 *
 * @param authenticationMethod
 * 		The authentication method
 *
 * @return the authentication method name
 */
const char *NDeviceCommon_Authentication_NAuthenticationMethod_GetName( NAuthenticationMethod authenticationMethod )
{
	return NAuthenticationMethodText[ authenticationMethod ];
}

/**
 * Find authentication method
 *
 * @param authenticationMethodName
 * 		The authentication method name
 *
 * @return authentication method or NAUTHENTICATION_METHODS
 */
NAuthenticationMethod NDeviceCommon_Authentication_NAuthenticationMethod_FindAuthentication( const char *authenticationMethodName )
{
	// Output
	__OUTPUT NAuthenticationMethod out = (NAuthenticationMethod)0;

	// Look for
	for( ; out < NAUTHENTICATION_METHODS; out++ )
		// Check
		if( NLib_Chaine_Comparer( authenticationMethodName,
			NAuthenticationMethodText[ out ],
			NTRUE,
			0 ) )
			// Found it!
			return out;

	// Not found
	return NAUTHENTICATION_METHODS;
}
