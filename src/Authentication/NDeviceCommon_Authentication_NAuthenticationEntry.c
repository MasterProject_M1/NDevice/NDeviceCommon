#include "../../include/NDeviceCommon.h"

// ----------------------------------------------------------
// struct NDeviceCommon::Authentication::NAuthenticationEntry
// ----------------------------------------------------------

/**
 * Resolve hostname (private)
 *
 * @param this
 * 		This instance
 *
 * @return if operation succeeded
 */
__PRIVATE NBOOL NDeviceCommon_Authentication_NAuthenticationEntry_ResolveHostname( NAuthenticationEntry *this )
{
	// Resolution list
	NListeResolution *resolutionList;

	// IP Detail
	const NDetailIP *ipDetail;

	// Resolve
	if( !( resolutionList = NLib_Module_Reseau_Resolution_NListeResolution_Construire( this->m_hostname,
		"80" ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_DNS_RESOLVE_FAILED );

		// Quit
		return NFALSE;
	}

	// Find IPv4
	if( !( ipDetail = NLib_Module_Reseau_Resolution_NListeResolution_ObtenirIPV4( resolutionList ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_DNS_RESOLVE_FAILED );

		// Destroy
		NLib_Module_Reseau_Resolution_NListeResolution_Detruire( &resolutionList );

		// Quit
		return NFALSE;
	}

	// Duplicate ip
	if( !( this->m_ip = NLib_Chaine_Dupliquer( NLib_Module_Reseau_IP_NDetailIP_ObtenirAdresse( ipDetail ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Destroy
		NLib_Module_Reseau_Resolution_NListeResolution_Detruire( &resolutionList );

		// Quit
		return NFALSE;
	}

	// Destroy
	NLib_Module_Reseau_Resolution_NListeResolution_Detruire( &resolutionList );

	// OK
	return NTRUE;
}

/**
 * Build authentication entry
 *
 * @param hostname
 * 		The remote hostname
 * @param port
 * 		The remote port
 * @param username
 * 		The remote username
 * @param authentication
 * 		The private authentication data
 * @param type
 * 		The entry type
 * @param method
 * 		The authentication method
 * @param isReadOnly
 * 		Read only entry?
 *
 * @return the instance
 */
__ALLOC NAuthenticationEntry *NDeviceCommon_Authentication_NAuthenticationEntry_Build( const char *hostname,
	NU32 port,
	const char *username,
	const char *authentication,
	NAuthenticationEntryType type,
	NAuthenticationMethod method,
	NBOOL isReadOnly )
{
	// Output
	__OUTPUT NAuthenticationEntry *out;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NAuthenticationEntry ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Save
	out->m_method = method;
	out->m_port = port;
	out->m_type = type;
	out->m_isReadOnly = isReadOnly;

	// Duplicate
	if( !( out->m_username = NLib_Chaine_Dupliquer( username ) )
		|| !( out->m_hostname = NLib_Chaine_Dupliquer( hostname ) )
		|| ( authentication != NULL
			&& !( out->m_authentication = NLib_Chaine_Dupliquer( authentication ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Free
		NFREE( out->m_hostname );
		NFREE( out->m_username );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Resolve hostname
	if( !NDeviceCommon_Authentication_NAuthenticationEntry_ResolveHostname( out ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_DNS_RESOLVE_FAILED );

		// Free
		NFREE( out->m_authentication );
		NFREE( out->m_hostname );
		NFREE( out->m_username );
		NFREE( out );

		// Quit
		return NULL;
	}

	// OK
	return out;
}

/**
 * Build authentication entry from file line
 *
 * @param line
 * 		The file line
 *
 * @return the instance
 */
__ALLOC NAuthenticationEntry *NDeviceCommon_Authentication_NAuthenticationEntry_Build2( const char *line )
{
	// Properties
	NListe *propertyList;

	// Username
	const char *username;

	// Authentication data
	const char *authentication;

	// Port
	NU32 port;

	// Hostname
	const char *hostname;

	// Element
	const char *element;

	// Is read only?
	NBOOL isReadOnly;

	// Type
	NAuthenticationEntryType entryType;

	// Method
	NAuthenticationMethod method;

	// Iterator
	NAuthenticationEntryProperty i;

	// Entry
	__OUTPUT NAuthenticationEntry *out = NULL;

	// Empty properties
	hostname = username = authentication = NULL;
	port = NERREUR;
	entryType = (NAuthenticationEntryType)NERREUR;
	method = (NAuthenticationMethod)NERREUR;
	isReadOnly = NERREUR;

	// Cut line
	if( ( propertyList = NLib_Chaine_Decouper( line,
		';' ) ) != NULL )
	{
		// Check count
		if( NLib_Memoire_NListe_ObtenirNombre( propertyList ) >= NAUTHENTICATION_ENTRY_PROPERTIES )
		{
			// Read properties
			for( i = (NAuthenticationEntryProperty)0; i < NLib_Memoire_NListe_ObtenirNombre( propertyList ); i++ )
			{
				// Get element
				element = NLib_Memoire_NListe_ObtenirElementDepuisIndex( propertyList,
					i );

				// Extract data
				switch( i )
				{
					default:
						break;

					case NAUTHENTICATION_ENTRY_PROPERTY_HOSTNAME:
						// Extract hostname
						hostname = element;
						break;
					case NAUTHENTICATION_ENTRY_PROPERTY_PORT:
						// Check port
						if( NLib_Chaine_EstUnNombreEntier( NLib_Memoire_NListe_ObtenirElementDepuisIndex( propertyList,
							i ),
							10 ) )
							port = (NU32)strtol( element,
								NULL,
								10 );
						break;
					case NAUTHENTICATION_ENTRY_PROPERTY_USERNAME:
						username = element;
						break;
					case NAUTHENTICATION_ENTRY_PROPERTY_PASSWORD:
						authentication = element;
						break;
					case NAUTHENTICATION_ENTRY_PROPERTY_TYPE:
						entryType = NGhostClient_Authentication_NAuthenticationEntryType_FindType( element );
						break;
					case NAUTHENTICATION_ENTRY_PROPERTY_METHOD:
						method = NDeviceCommon_Authentication_NAuthenticationMethod_FindAuthentication( element );
						break;
					case NAUTHENTICATION_ENTRY_PROPERTY_IS_READ_ONLY:
						isReadOnly = NLib_Type_NBOOL_Parse( element );
						break;
				}
			}

			// Check what has been read
			if( hostname != NULL
				&& username != NULL
				&& authentication != NULL
				&& port != NERREUR
				&& entryType != NERREUR
				&& method != NERREUR
				&& isReadOnly != NERREUR )
				// Create entry
				out = NDeviceCommon_Authentication_NAuthenticationEntry_Build( hostname,
					port,
					username,
					NLib_Chaine_Comparer( authentication,
						"NULL",
						NTRUE,
						0 ) ?
						NULL
						: authentication,
					entryType,
					method,
					isReadOnly );
		}

		// Destroy
		NLib_Memoire_NListe_Detruire( &propertyList );
	}

	// Check
	if( !out )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NULL;
	}

	// OK
	return out;
}

/**
 * Destroy authentication entry
 *
 * @param this
 * 		This instance
 */
void NDeviceCommon_Authentication_NAuthenticationEntry_Destroy( NAuthenticationEntry **this )
{
	// Free
	NFREE( (*this)->m_authentication );
	NFREE( (*this)->m_ip );
	NFREE( (*this)->m_hostname );
	NFREE( (*this)->m_username );
	NFREE( (*this) );
}

/**
 * Get entry type
 *
 * @param this
 * 		This instance
 *
 * @return the entry type
 */
NAuthenticationEntryType NDeviceCommon_Authentication_NAuthenticationEntry_GetType( const NAuthenticationEntry *this )
{
	return this->m_type;
}

/**
 * Get authentication method
 *
 * @param this
 * 		This instance
 *
 * @return the authentication method
 */
NAuthenticationMethod NDeviceCommon_Authentication_NAuthenticationEntry_GetMethod( const NAuthenticationEntry *this )
{
	return this->m_method;
}

/**
 * Get hostname
 *
 * @param this
 * 		This instance
 *
 * @return the hostname
 */
const char *NDeviceCommon_Authentication_NAuthenticationEntry_GetHostname( const NAuthenticationEntry *this )
{
	return this->m_hostname;
}

/**
 * Generate json builder compatible host
 *
 * @param this
 * 		This instance
 *
 * @return the json compatible hostname
 */
__ALLOC char *NDeviceCommon_Authentication_NAuthenticationEntry_GenerateJsonBuilderCompatibleHostname( const NAuthenticationEntry *this )
{
	// Output
	__OUTPUT char *out;

	// Duplicate hostname
	if( !( out = NLib_Chaine_Dupliquer( this->m_hostname ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Quit
		return NULL;
	}

	// Replace '.'
	NLib_Chaine_Remplacer( out,
		'.',
		NDEVICECOMMON_TEMPORARY_POINT_CHARACTER_REPLACEMENT_CHARACTER );

	// OK
	return out;
}

/**
 * Get ip
 *
 * @param this
 * 		This instance
 *
 * @return the ip
 */
const char *NDeviceCommon_Authentication_NAuthenticationEntry_GetIP( const NAuthenticationEntry *this )
{
	return this->m_ip;
}

/**
 * Get port
 *
 * @param this
 * 		This instance
 *
 * @return the port
 */
NU32 NDeviceCommon_Authentication_NAuthenticationEntry_GetPort( const NAuthenticationEntry *this )
{
	return this->m_port;
}

/**
 * Get username
 *
 * @param this
 * 		This instance
 *
 * @return the username
 */
const char *NDeviceCommon_Authentication_NAuthenticationEntry_GetUsername( const NAuthenticationEntry *this )
{
	return this->m_username;
}

/**
 * Get authentication
 *
 * @param this
 * 		This instance
 *
 * @return the authentication
 */
const char *NDeviceCommon_Authentication_NAuthenticationEntry_GetAuthentication( const NAuthenticationEntry *this )
{
	return this->m_authentication;
}

/**
 * Is read only
 *
 * @param this
 * 		This instance
 *
 * @return if the entry if read only
 */
NBOOL NDeviceCommon_Authentication_NAuthenticationEntry_IsReadOnly( const NAuthenticationEntry *this )
{
	return this->m_isReadOnly;
}

/**
 * Set read only
 *
 * @param this
 * 		This instance
 */
void NDeviceCommon_Authentication_NAuthenticationEntry_SetReadOnly( NAuthenticationEntry *this )
{
	this->m_isReadOnly = NTRUE;
}

/**
 * Update from PUT request
 *
 * @param this
 * 		This instance
 * @param clientData
 * 		The client data
 * @param clientDataLength
 *		The client data length
 *
 * @return if operation succeeded
 */
NBOOL NDeviceCommon_Authentication_NAuthenticationEntry_Update( NAuthenticationEntry *this,
	const char *clientData,
	NU32 clientDataLength )
{
	// Parser output list
	NParserOutputList *parserOutputList;

	// Port
	NU32 port = NERREUR;

	// Authentication entry type
	NAuthenticationEntryType type = (NAuthenticationEntryType)NERREUR;

	// Authentication method
	NAuthenticationMethod method = (NAuthenticationMethod)NERREUR;

	// Hostname
	char *hostname = NULL;

	// Username
	char *user = NULL;

	// Private data
	char *privateData = NULL;

	// Check if read only
	if( this->m_isReadOnly )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_OBJECT_STATE );

		// Quit
		return NFALSE;
	}

	// Parse
	if( !( parserOutputList = NJson_Engine_Parser_Parse( clientData,
		clientDataLength ) ) )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Quit
		return NFALSE;
	}

	// Extract data
	if( !NDeviceCommon_Service_Authentication_ExtractUserDataFromParserOutputList( parserOutputList,
		NULL,
		&port,
		&type,
		&method,
		&hostname,
		&user,
		&privateData,
		NFALSE ) )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Destroy
		NParser_Output_NParserOutputList_Destroy( &parserOutputList );

		// Quit
		return NFALSE;
	}

	// Update port
	if( port != NERREUR )
		this->m_port = port;

	// Update method
	if( method != NERREUR )
		this->m_method = method;

	// Update type
	if( type != NERREUR )
		this->m_type = type;

	// Update hostname
	if( hostname != NULL )
	{
		// Free
		NFREE( this->m_hostname );

		// Save
		this->m_hostname = hostname;
	}

	// Update user
	if( user != NULL )
	{
		// Free
		NFREE( this->m_username );

		// Save
		this->m_username = user;
	}

	// Update private data
	if( privateData != NULL )
	{
		// Update
		switch( this->m_method )
		{
			case NAUTHENTICATION_METHOD_NO_AUTHENTICATION:
			default:
				// Free
				NFREE( privateData );
				break;

			case NAUTHENTICATION_METHOD_PASSWORD:
			case NAUTHENTICATION_METHOD_PRIVATE_KEY:
				// Free
				NFREE( this->m_authentication );

				// Save
				this->m_authentication = privateData;
				break;
		}
	}

	// Destroy parser output list
	NParser_Output_NParserOutputList_Destroy( &parserOutputList );

	// OK
	return NTRUE;
}

/**
 * Save entry on disk
 *
 * @param this
 * 		This instance
 * @param file
 * 		The file instance
 *
 * @return if operation succeeded
 */
NBOOL NDeviceCommon_Authentication_NAuthenticationEntry_Save( const NAuthenticationEntry *this,
	NFichierTexte *file )
{
	// Property
	NAuthenticationEntryProperty property = (NAuthenticationEntryProperty)0;

	// Write properties
	for( ; property < NAUTHENTICATION_ENTRY_PROPERTIES; property++ )
	{
		// Write data
		switch( property )
		{
			default:
				break;

			case NAUTHENTICATION_ENTRY_PROPERTY_HOSTNAME:
				NLib_Fichier_NFichierTexte_Ecrire3( file,
					this->m_hostname );
				break;
			case NAUTHENTICATION_ENTRY_PROPERTY_PORT:
				NLib_Fichier_NFichierTexte_Ecrire( file,
					this->m_port );
				break;
			case NAUTHENTICATION_ENTRY_PROPERTY_USERNAME:
				NLib_Fichier_NFichierTexte_Ecrire3( file,
					this->m_username );
				break;
			case NAUTHENTICATION_ENTRY_PROPERTY_PASSWORD:
				NLib_Fichier_NFichierTexte_Ecrire3( file,
					this->m_authentication == NULL ?
						"NULL"
						: this->m_authentication );
				break;
			case NAUTHENTICATION_ENTRY_PROPERTY_TYPE:
				NLib_Fichier_NFichierTexte_Ecrire3( file,
					NGhostClient_Authentication_NAuthenticationEntryType_GetName( this->m_type ) );
				break;
			case NAUTHENTICATION_ENTRY_PROPERTY_METHOD:
				NLib_Fichier_NFichierTexte_Ecrire3( file,
					NDeviceCommon_Authentication_NAuthenticationMethod_GetName( this->m_method ) );
				break;
			case NAUTHENTICATION_ENTRY_PROPERTY_IS_READ_ONLY:
				NLib_Fichier_NFichierTexte_Ecrire3( file,
					this->m_isReadOnly ?
						NTRUE_KEYWORD
						: NFALSE_KEYWORD );
		}

		// Write separation character
		NLib_Fichier_NFichierTexte_Ecrire6( file,
			';' );
	}

	// Separate line
	NLib_Fichier_NFichierTexte_Ecrire3( file,
		"\n" );

	// OK
	return NTRUE;
}

/**
 * Build parser output entry
 *
 * @param this
 * 		This instance
 * @param root
 * 		The key root
 * @param parserOutputList
 * 		The parser output list
 * @param authenticationEntryServiceType
 * 		The service
 *
 * @return if the operation succeeded
 */
NBOOL NDeviceCommon_Authentication_NAuthenticationEntry_BuildParserOutputListInternal( const NAuthenticationEntry *this,
	const char *root,
	__OUTPUT NParserOutputList *parserOutputList,
	NU32 authenticationEntryServiceType )
{
	// Key
	char key[ 128 ];

	// Build key
	snprintf( key,
		128,
		"%s%s%s",
		root,
		strlen( root ) > 0 ?
			"."
			: "",
		NDeviceCommon_Service_Authentication_NAuthenticationEntryServiceType_GetName( (NAuthenticationEntryServiceType)authenticationEntryServiceType ) );

	// Add property
	switch( authenticationEntryServiceType )
	{
		case NAUTHENTICATION_ENTRY_SERVICE_TYPE_HOSTNAME:
			if( NDeviceCommon_Authentication_NAuthenticationEntry_GetHostname( this ) != NULL )
				return NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
					key,
					NDeviceCommon_Authentication_NAuthenticationEntry_GetHostname( this ) );
			break;
		case NAUTHENTICATION_ENTRY_SERVICE_TYPE_PORT:
			return NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
				key,
				NDeviceCommon_Authentication_NAuthenticationEntry_GetPort( this ) );

		case NAUTHENTICATION_ENTRY_SERVICE_TYPE_USER:
			if( NDeviceCommon_Authentication_NAuthenticationEntry_GetUsername( this ) != NULL )
				return NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
					key,
					NDeviceCommon_Authentication_NAuthenticationEntry_GetUsername( this ) );
			break;
		case NAUTHENTICATION_ENTRY_SERVICE_TYPE_TYPE:
			return NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
				key,
				NGhostClient_Authentication_NAuthenticationEntryType_GetName( NDeviceCommon_Authentication_NAuthenticationEntry_GetType( this ) ) );

		case NAUTHENTICATION_ENTRY_SERVICE_TYPE_METHOD:
			return NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
				key,
				NDeviceCommon_Authentication_NAuthenticationMethod_GetName( NDeviceCommon_Authentication_NAuthenticationEntry_GetMethod( this ) ) );

		case NAUTHENTICATION_ENTRY_SERVICE_TYPE_PRIVATE_DATA:
			if( NDeviceCommon_Authentication_NAuthenticationEntry_GetAuthentication( this ) != NULL )
				return NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
					key,
					NDeviceCommon_Authentication_NAuthenticationEntry_GetAuthentication( this ) );
			break;

		default:
			break;
	}

	// Failed
	return NTRUE;
}

/**
 * Build parser output list
 *
 * @param this
 * 		This instance
 * @param root
 * 		The key root
 * @param parserOutputList
 * 		The parser output list
 *
 * @return if the operation succeeded
 */
NBOOL NDeviceCommon_Authentication_NAuthenticationEntry_BuildParserOutputList( const NAuthenticationEntry *this,
	const char *root,
	__OUTPUT NParserOutputList *parserOutputList )
{
	// Authentication entry property
	NAuthenticationEntryServiceType authenticationEntryServiceType;

	// Iterate properties
	for( authenticationEntryServiceType = (NAuthenticationEntryServiceType)1; authenticationEntryServiceType < NAUTHENTICATION_ENTRY_SERVICE_TYPES; authenticationEntryServiceType++ )
		NDeviceCommon_Authentication_NAuthenticationEntry_BuildParserOutputListInternal( this,
			root,
			parserOutputList,
			authenticationEntryServiceType );

	// OK
	return NTRUE;
}
