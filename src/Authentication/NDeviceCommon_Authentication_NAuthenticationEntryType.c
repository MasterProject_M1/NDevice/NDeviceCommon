#define NGHOSTCLIENT_AUTHENTICATION_NAUTHENTICATIONENTRYTYPE_INTERNE
#include "../../include/NDeviceCommon.h"

// -----------------------------------------------------------
// enum NGhostClient::Authentication::NAuthenticationEntryType
// -----------------------------------------------------------

/**
 * Get entry type name
 *
 * @param entryType
 * 		The entry type
 *
 * @return the entry type name
 */
const char *NGhostClient_Authentication_NAuthenticationEntryType_GetName( NAuthenticationEntryType entryType )
{
	return NAuthenticationEntryTypeText[ entryType ];
}

/**
 * Find entry type
 *
 * @param entryName
 * 		The name
 *
 * @return entry type or NAUTHENTICATION_ENTRY_TYPES
 */
NAuthenticationEntryType NGhostClient_Authentication_NAuthenticationEntryType_FindType( const char *entryName )
{
	// Output
	__OUTPUT NAuthenticationEntryType out = (NAuthenticationEntryType)0;

	// Look for
	for( ; out < NAUTHENTICATION_ENTRY_TYPES; out++ )
		// Check
		if( NLib_Chaine_Comparer( entryName,
			NAuthenticationEntryTypeText[ out ],
			NTRUE,
			0 ) )
			// Found it
			return out;

	// Not found
	return NAUTHENTICATION_ENTRY_TYPES;
}

