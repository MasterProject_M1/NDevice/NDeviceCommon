#include "../include/NDeviceCommon.h"

// -----------------------
// namespace NDeviceCommon
// -----------------------

/**
 * Display header
 *
 * @param name
 * 		The program name
 * @param listeningPort
 * 		The listening port
 * @param ip
 * 		The device IP
 */
void NDeviceCommon_DisplayHeader( const char *name,
	NU32 listeningPort,
	const char *ip )
{
	// Iterator
	NU32 i;

	// Limit
	NU32 limit;

	// Buffer
	char buffer[ 128 ];

#define LINE_LENGTH_DIVIDED		26

	// Ghost header
	puts( "                                                      " );
	puts( "              ('-. .-.              .-')   .-') _     " );
	puts( "             ( OO )  /             ( OO ).(  OO) )    " );
	puts( "    ,----.   ,--. ,--..-'),-----. (_)---\\_)     '._  " );
	puts( "   '  .-./-')|  | |  ( OO'  .-.  '/    _ ||'--...__)  " );
	puts( "   |  |_( O- )   .|  /   |  | |  |\\  :` `.'--.  .--' " );
	puts( "   |  | .--, \\       \\_) |  |\\|  | '..`''.)  |  |  " );
	puts( "  (|  | '. (_/  .-.  | \\ |  | |  |.-._)   \\  |  |   " );
	puts( "   |  '--'  ||  | |  |  `'  '-'  '\\       /  |  |    " );
	puts( "    `------' `--' `--'    `-----'  `-----'   `--'     " );
	puts( "                                                      " );

	// Build shared ip
	snprintf( buffer,
		128,
		"Shared ip is %s",
		ip );

	// Calculate limit
	limit = LINE_LENGTH_DIVIDED - ( (NU32)strlen( buffer ) / 2 );

	// Shared ip spaces
	for( i = 0; (NS32)i < (NS32)limit; i++ )
		printf( " " );

	// Display shared ip
	puts( buffer );
	puts( "" );
	// Calculate limit
	limit = LINE_LENGTH_DIVIDED - ( (NU32)strlen( name ) / 2 );

	// Program name spaces
	for( i = 0; (NS32)i < (NS32)limit; i++ )
		printf( " " );

	// Display program name
	puts( name );

	// Separator spaces
	puts( "                         - " );

	// Build port
	snprintf( buffer,
		128,
		"Now listening on %d",
		listeningPort );

	// Calculate limit
	limit = LINE_LENGTH_DIVIDED - ( (NU32)strlen( buffer ) / 2 );

	// Listening port spaces
	for( i = 0; (NS32)i < (NS32)limit; i++ )
		printf( " " );

	// Display listening port
	puts( buffer );

	// Separate
	puts( "" );
#undef LINE_LENGTH_DIVIDED
}

