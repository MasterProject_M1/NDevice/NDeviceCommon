#define NDEVICECOMMON_TYPE_NDEVICETYPE_INTERNE
#include "../../include/NDeviceCommon.h"

// -------------------------------------
// enum NDeviceCommon::Type::NDeviceType
// -------------------------------------

/**
 * Is it a ghost device?
 *
 * @param deviceType
 * 		The device type
 *
 * @return if this is a ghost device
 */
NBOOL NDeviceCommon_Type_NDeviceType_IsGhostDevice( NDeviceType deviceType )
{
	switch( deviceType )
	{
		case NDEVICE_TYPE_HUE:
		case NDEVICE_TYPE_MFI_MPOWER:
			return NFALSE;

		default:
			return NTRUE;
	}
}

/**
 * Get device user friendly type name
 *
 * @param deviceType
 * 		The device type
 *
 * @return the device type name
 */
const char *NDeviceCommon_Type_NDeviceType_GetUserFriendlyName( NDeviceType deviceType )
{
	return NDeviceTypeUserFriendlyName[ deviceType ];
}

/**
 * Get device formal name
 *
 * @param deviceType
 * 		The device type
 *
 * @return the formal name
 */
const char *NDeviceCommon_Type_NDeviceType_GetName( NDeviceType deviceType )
{
	return NDeviceTypeName[ deviceType ];
}

/**
 * Get device listening port
 *
 * @param deviceType
 * 		The device type
 *
 * @return the device listening port
 */
NU32 NDeviceCommon_Type_NDeviceType_GetListeningPort( NDeviceType deviceType )
{
	return NDeviceTypeListeningPort[ deviceType ];
}

/**
 * Get element to GET to check if a device has specified type
 *
 * @param deviceType
 * 		The device type
 *
 * @return the GET element
 */
const char *NDeviceCommon_Type_NDeviceType_GetElementToGET( NDeviceType deviceType )
{
	return NDeviceTypeElementGET[ deviceType ];
}

/**
 * Find device through type name (case insensitive)
 *
 * @param typeName
 * 		The type name
 *
 * @return the device type
 */
NDeviceType NDeviceCommon_Type_NDeviceType_FindTypeFromName( const char *typeName )
{
	// Iterator
	__OUTPUT NDeviceType out = (NDeviceType)0;

	// Look for
	for( ; out < NDEVICES_TYPE; out++ )
		// Check
		if( NLib_Chaine_Comparer( NDeviceTypeName[ out ],
			typeName,
			NFALSE,
			0 ) )
			// Found it
			return out;

	// Not found
	return NDEVICES_TYPE;
}

/**
 * Get device token header
 *
 * @param deviceType
 * 		The device type
 *
 * @return the short header for token
 */
const char *NDeviceCommon_Type_NDeviceType_GetShortHeaderName( NDeviceType deviceType )
{
	return NDeviceTypeTokenHeaderName[ deviceType ];
}

/**
 * Get device type id modifier
 *
 * @param deviceType
 * 		The device type
 *
 * @return the ghost device modifier
 */
NU64 NDeviceCommon_Type_NDeviceType_GetIdModifier( NDeviceType deviceType )
{
	return NDeviceTypeIDModifier[ deviceType ];
}
