#include "../../include/NDeviceCommon.h"

// -----------------------------
// namespace NDeviceCommon::UUID
// -----------------------------

/**
 * Convert uuid to number
 *
 * @param uuid
 * 		The uuid
 *
 * @return the converted uuid or 0xFFFFFFFFFFFFFFFF
 */
NU64 NDeviceCommon_UUID_ConvertUUIDToNumber( const char *uuid )
{
	// Output
	__OUTPUT NU64 output;

	// Cursor
	NU32 cursor = 0;

	// UUID length
	NU32 length;

	// Separator list
	const char *separatorList = ":-";

	// Number part
	NU64 numberPart;

	// Buffer
	char *buffer;

	// Iterators
	NU32 i,
		j;

	// Separator count
	NU32 separatorCount;

	// AABBCCDDEEFF
	if( !NLib_Chaine_EstChaineContient( uuid,
			separatorList[ 0 ] )
		&& !NLib_Chaine_EstChaineContient( uuid,
		separatorList[ 1 ] ) )
	{
		// Is it a number
		if( !NLib_Chaine_EstUnNombreEntier( uuid,
			16 ) )
		{
			// Notify
			NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

			// Quit
			return (NU64)0xFFFFFFFFFFFFFFFF;
		}

		// Convert
		return (NU64)strtoll( uuid,
			NULL,
			16 );
	}

	// Check length
	if( ( length = (NU32)strlen( uuid ) ) != NTAILLE_ADRESSE_MAC
		&& length != 19 )
		return (NU64)0xFFFFFFFFFFFFFFFF;

	// Count separators
	separatorCount = (NU32)strlen( separatorList );

	// Other forms
	for( i = 0; i < separatorCount; i++ )
	{
		// Zero
		cursor = 0;
		output = 0;

		// Convert
		if( NLib_Chaine_EstChaineContient( uuid,
			separatorList[ i ] ) )
			switch( NLib_Chaine_CompterNombreOccurence( uuid,
					separatorList[ i ] ) )
			{
				case NNOMBRE_SEPARATEUR_ADRESSE_MAC:
					// AA?BB?CC?DD?EE?FF
					for( j = 0; j <= NNOMBRE_SEPARATEUR_ADRESSE_MAC; j++ )
					{
						// Read until separator
						if( !( buffer = NLib_Chaine_LireJusqua( uuid,
							separatorList[ i ],
							&cursor,
							NFALSE ) )
							|| strlen( buffer ) != 2 )
						{
							// Notify
							NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

							// Free
							NFREE( buffer );

							// Quit
							return (NU64)0xFFFFFFFFFFFFFFFF;
						}


						// Check if this is a number
						if( !NLib_Chaine_EstUnNombreEntier( buffer,
							16 ) )
						{
							// Notify
							NOTIFIER_ERREUR( NERREUR_SYNTAX );

							// Free
							NFREE( buffer );

							// Quit
							return (NU64)0xFFFFFFFFFFFFFFFF;
						}

						// Convert
						if( ( numberPart = (NU64)strtoll( buffer,
							NULL,
							16 ) ) > 0xFF )
						{
							// Notify
							NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

							// Free
							NFREE( buffer );

							// Quit
							return (NU64)0xFFFFFFFFFFFFFFFF;
						}

						// Free
						NFREE( buffer );

						// Add to output
						output |= ( ( numberPart ) << ( ( NNOMBRE_SEPARATEUR_ADRESSE_MAC - j ) * 8 ) );
					}

					// OK
					return output;

				case 3:
					// AAAA?BBBB?CCCC?DDDD
					for( j = 0; j <= 3; j++ )
					{
						// Read until separator
						if( !( buffer = NLib_Chaine_LireJusqua( uuid,
							separatorList[ i ],
							&cursor,
							NFALSE ) )
							|| strlen( buffer ) != 4 )
						{
							// Notify
							NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

							// Free
							NFREE( buffer );

							// Quit
							return (NU64)0xFFFFFFFFFFFFFFFF;
						}

						// Check if this is a number
						if( !NLib_Chaine_EstUnNombreEntier( buffer,
							16 ) )
						{
							// Notify
							NOTIFIER_ERREUR( NERREUR_SYNTAX );

							// Free
							NFREE( buffer );

							// Quit
							return (NU64)0xFFFFFFFFFFFFFFFF;
						}

						// Convert
						if( ( numberPart = (NU64)strtoll( buffer,
							NULL,
							16 ) ) > 0xFFFF )
						{
							// Notify
							NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

							// Free
							NFREE( buffer );

							// Quit
							return (NU64)0xFFFFFFFFFFFFFFFF;
						}

						// Free
						NFREE( buffer );

						// Add to output
						output |= ( ( numberPart ) << ( ( 3 - j ) * 16 ) );
					}

					// OK
					return output;

				default:
					break;
			}
		}

	// OK
	return (NU64)0xFFFFFFFFFFFFFFFF;
}

/**
 * Convert number to uuid
 *
 * @param number
 * 		The number to convert
 *
 * @return the uuid or NULL
 */
__ALLOC char *NDeviceCommon_UUID_ConvertNumberToUUID( NU64 number )
{
	// Output
	char *output = NULL;
	NU32 outputSize = 0;

	// Buffer
	char buffer[ 32 ];

	// Iterator
	NU32 i = 0;

	// Iterate
	for( ; i <= 3; i++ )
	{
		// Clear buffer
		memset( buffer,
			0,
			32 );

		// Write to buffer
		snprintf( buffer,
			32,
			"%04llX%s",
			( number >> ( ( 3 - i ) * 16 ) & 0x0000FFFF ),
			i + 1 > 3 ?
				""
				: "-" );

		// Add to output
		NLib_Memoire_AjouterData2( &output,
			outputSize,
			buffer );

		// Increase size
		outputSize += strlen( buffer );
	}

	// OK
	return output;
}

