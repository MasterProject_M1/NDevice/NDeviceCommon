#ifndef NDEVICECOMMON_PROTECT
#define NDEVICECOMMON_PROTECT

// -----------------------
// namespace NDeviceCommon
// -----------------------

// namespace NLib
#include "../../../NLib/NLib/NLib/include/NLib/NLib.h"

// namespace NHTTP
#include "../../../NLib/NHTTP/include/NHTTP.h"

// namespace NParser
#include "../../../NParser/NParser/include/NParser.h"

// namespace NJson
#include "../../../NParser/NJson/include/NJson.h"

// namespace NDeviceCommon::Type
#include "Type/NDeviceCommon_Type.h"

// namespace NDeviceCommon::UUID
#include "UUID/NDeviceCommon_UUID.h"

// namespace NDeviceCommon::Authentication
#include "Authentication/NDeviceCommon_Authentication.h"

// namespace NDeviceCommon::Service
#include "Service/NDeviceCommon_Service.h"

// Authentication failure realm message
#define NDEVICE_COMMON_AUTHENTICATION_FAILURE_REALM_MESSAGE					"You have to authenticate to access this service: "

// Point character correction
#define NDEVICECOMMON_TEMPORARY_POINT_CHARACTER_REPLACEMENT_CHARACTER		(char)255

/**
 * Display header
 *
 * @param name
 * 		The program name
 * @param listeningPort
 * 		The listening port
 * @param ip
 * 		The device IP
 */
void NDeviceCommon_DisplayHeader( const char *name,
	NU32 listeningPort,
	const char *ip );

#endif // !NDEVICECOMMON_PROTECT

