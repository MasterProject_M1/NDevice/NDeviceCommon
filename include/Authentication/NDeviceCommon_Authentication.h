#ifndef NDEVICECOMMON_AUTHENTICATION_PROTECT
#define NDEVICECOMMON_AUTHENTICATION_PROTECT

// ---------------------------------------
// namespace NDeviceCommon::Authentication
// ---------------------------------------

// Broadcast connection timeout (ms)
#define NDEVICE_COMMON_AUTHENTICATION_BROADCAST_CONNECTION_TIMEOUT		500

// enum NDeviceCommon::Authentication::NAuthenticationEntryType
#include "NDeviceCommon_Authentication_NAuthenticationEntryType.h"

// enum NDeviceCommon::Authentication::NAuthenticationMethod
#include "NDeviceCommon_Authentication_NAuthenticationMethod.h"

// enum NDeviceCommon::Authentication::NAuthenticationEntryProperty
#include "NDeviceCommon_Authentication_NAuthenticationEntryProperty.h"

// struct NDeviceCommon::Authentication::NAuthenticationEntry
#include "NDeviceCommon_Authentication_NAuthenticationEntry.h"

// struct NDeviceCommon::Authentication::NAuthenticationManager
#include "NDeviceCommon_Authentication_NAuthenticationManager.h"

#endif // !NDEVICECOMMON_AUTHENTICATION_PROTECT

