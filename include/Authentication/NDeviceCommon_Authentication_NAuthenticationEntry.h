#ifndef NDEVICECOMMON_AUTHENTICATION_NAUTHENTICATIONENTRY_PROTECT
#define NDEVICECOMMON_AUTHENTICATION_NAUTHENTICATIONENTRY_PROTECT

// ----------------------------------------------------------
// struct NDeviceCommon::Authentication::NAuthenticationEntry
// ----------------------------------------------------------

typedef struct NAuthenticationEntry
{
	// Authentication type
	NAuthenticationEntryType m_type;

	// Authentication method
	NAuthenticationMethod m_method;

	// Hostname
	char *m_hostname;

	// IP
	char *m_ip;

	// Port
	NU32 m_port;

	// Username
	char *m_username;

	// Authentication
	char *m_authentication;

	// Is read only?
	NBOOL m_isReadOnly;
} NAuthenticationEntry;

/**
 * Build authentication entry
 *
 * @param hostname
 * 		The remote hostname
 * @param port
 * 		The remote port
 * @param username
 * 		The remote username
 * @param authentication
 * 		The private authentication data
 * @param type
 * 		The entry type
 * @param method
 * 		The authentication method
 *
 * @return the instance
 */
__ALLOC NAuthenticationEntry *NDeviceCommon_Authentication_NAuthenticationEntry_Build( const char *hostname,
	NU32 port,
	const char *username,
	const char *authentication,
	NAuthenticationEntryType type,
	NAuthenticationMethod method,
	NBOOL isReadOnly );

/**
 * Build authentication entry from file line
 *
 * @param line
 * 		The file line
 *
 * @return the instance
 */
__ALLOC NAuthenticationEntry *NDeviceCommon_Authentication_NAuthenticationEntry_Build2( const char *line );

/**
 * Destroy authentication entry
 *
 * @param this
 * 		This instance
 */
void NDeviceCommon_Authentication_NAuthenticationEntry_Destroy( NAuthenticationEntry** );

/**
 * Get entry type
 *
 * @param this
 * 		This instance
 *
 * @return the entry type
 */
NAuthenticationEntryType NDeviceCommon_Authentication_NAuthenticationEntry_GetType( const NAuthenticationEntry* );

/**
 * Get authentication method
 *
 * @param this
 * 		This instance
 *
 * @return the authentication method
 */
NAuthenticationMethod NDeviceCommon_Authentication_NAuthenticationEntry_GetMethod( const NAuthenticationEntry* );

/**
 * Get hostname
 *
 * @param this
 * 		This instance
 *
 * @return the hostname
 */
const char *NDeviceCommon_Authentication_NAuthenticationEntry_GetHostname( const NAuthenticationEntry* );

/**
 * Generate json builder compatible host
 *
 * @param this
 * 		This instance
 *
 * @return the json compatible hostname
 */
__ALLOC char *NDeviceCommon_Authentication_NAuthenticationEntry_GenerateJsonBuilderCompatibleHostname( const NAuthenticationEntry* );

/**
 * Get ip
 *
 * @param this
 * 		This instance
 *
 * @return the ip
 */
const char *NDeviceCommon_Authentication_NAuthenticationEntry_GetIP( const NAuthenticationEntry* );

/**
 * Get port
 *
 * @param this
 * 		This instance
 *
 * @return the port
 */
NU32 NDeviceCommon_Authentication_NAuthenticationEntry_GetPort( const NAuthenticationEntry* );

/**
 * Get username
 *
 * @param this
 * 		This instance
 *
 * @return the username
 */
const char *NDeviceCommon_Authentication_NAuthenticationEntry_GetUsername( const NAuthenticationEntry* );

/**
 * Get authentication
 *
 * @param this
 * 		This instance
 *
 * @return the authentication
 */
const char *NDeviceCommon_Authentication_NAuthenticationEntry_GetAuthentication( const NAuthenticationEntry* );

/**
 * Is read only
 *
 * @param this
 * 		This instance
 *
 * @return if the entry if read only
 */
NBOOL NDeviceCommon_Authentication_NAuthenticationEntry_IsReadOnly( const NAuthenticationEntry* );

/**
 * Set read only
 *
 * @param this
 * 		This instance
 */
void NDeviceCommon_Authentication_NAuthenticationEntry_SetReadOnly( NAuthenticationEntry* );

/**
 * Update from PUT request
 *
 * @param this
 * 		This instance
 * @param clientData
 * 		The client data
 * @param clientDataLength
 *		The client data length
 *
 * @return if operation succeeded
 */
NBOOL NDeviceCommon_Authentication_NAuthenticationEntry_Update( NAuthenticationEntry*,
	const char *clientData,
	NU32 clientDataLength );

/**
 * Save entry on disk
 *
 * @param this
 * 		This instance
 * @param file
 * 		The file instance
 *
 * @return if operation succeeded
 */
NBOOL NDeviceCommon_Authentication_NAuthenticationEntry_Save( const NAuthenticationEntry*,
	NFichierTexte *file );

/**
 * Build parser output entry
 *
 * @param this
 * 		This instance
 * @param root
 * 		The key root
 * @param parserOutputList
 * 		The parser output list
 * @param authenticationEntryServiceType
 * 		The service
 *
 * @return if the operation succeeded
 */
NBOOL NDeviceCommon_Authentication_NAuthenticationEntry_BuildParserOutputListInternal( const NAuthenticationEntry*,
	const char *root,
	__OUTPUT NParserOutputList *parserOutputList,
	NU32 authenticationEntryServiceType );

/**
 * Build parser output list
 *
 * @param this
 * 		This instance
 * @param root
 * 		The key root
 * @param parserOutputList
 * 		The parser output list
 *
 * @return if the operation succeeded
 */
NBOOL NDeviceCommon_Authentication_NAuthenticationEntry_BuildParserOutputList( const NAuthenticationEntry*,
	const char *root,
	__OUTPUT NParserOutputList *parserOutputList );

#endif // !NDEVICECOMMON_AUTHENTICATION_NAUTHENTICATIONENTRY_PROTECT
