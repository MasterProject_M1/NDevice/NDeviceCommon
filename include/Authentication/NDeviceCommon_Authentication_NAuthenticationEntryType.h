#ifndef NGHOSTCLIENT_AUTHENTICATION_NAUTHENTICATIONENTRYTYPE_PROTECT
#define NGHOSTCLIENT_AUTHENTICATION_NAUTHENTICATIONENTRYTYPE_PROTECT

// -----------------------------------------------------------
// enum NGhostClient::Authentication::NAuthenticationEntryType
// -----------------------------------------------------------

typedef enum NAuthenticationEntryType
{
	NAUTHENTICATION_ENTRY_TYPE_SSH_SFTP,
	NAUTHENTICATION_ENTRY_TYPE_HTTP_BASIC,
	NAUTHENTICATION_ENTRY_TYPE_GHOST_TOKEN,

	NAUTHENTICATION_ENTRY_TYPES
} NAuthenticationEntryType;

/**
 * Get entry type name
 *
 * @param entryType
 * 		The entry type
 *
 * @return the entry type name
 */
const char *NGhostClient_Authentication_NAuthenticationEntryType_GetName( NAuthenticationEntryType entryType );

/**
 * Find entry type
 *
 * @param entryName
 * 		The name
 *
 * @return entry type or NAUTHENTICATION_ENTRY_TYPES
 */
NAuthenticationEntryType NGhostClient_Authentication_NAuthenticationEntryType_FindType( const char *entryName );

#ifdef NGHOSTCLIENT_AUTHENTICATION_NAUTHENTICATIONENTRYTYPE_INTERNE
static const char NAuthenticationEntryTypeText[ NAUTHENTICATION_ENTRY_TYPES ][ 32 ] =
{
	"SSH/SFTP",
	"HTTPBasic",
	"GhostToken"
};
#endif // NGHOSTCLIENT_AUTHENTICATION_NAUTHENTICATIONENTRYTYPE_INTERNE

#endif // !NGHOSTCLIENT_AUTHENTICATION_NAUTHENTICATIONENTRYTYPE_PROTECT
