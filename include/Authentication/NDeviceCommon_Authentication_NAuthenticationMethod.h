#ifndef NDEVICECOMMON_AUTHENTICATION_NAUTHENTICATIONMETHOD_PROTECT
#define NDEVICECOMMON_AUTHENTICATION_NAUTHENTICATIONMETHOD_PROTECT

// ---------------------------------------------------------
// enum NDeviceCommon::Authentication::NAuthenticationMethod
// ---------------------------------------------------------

typedef enum NAuthenticationMethod
{
	NAUTHENTICATION_METHOD_NO_AUTHENTICATION,

	NAUTHENTICATION_METHOD_PASSWORD,
	NAUTHENTICATION_METHOD_PRIVATE_KEY,

	NAUTHENTICATION_METHOD_TOKEN,

	NAUTHENTICATION_METHODS
} NAuthenticationMethod;

/**
 * Get authentication method name
 *
 * @param authenticationMethod
 * 		The authentication method
 *
 * @return the authentication method name
 */
const char *NDeviceCommon_Authentication_NAuthenticationMethod_GetName( NAuthenticationMethod authenticationMethod );

/**
 * Find authentication method
 *
 * @param authenticationMethodName
 * 		The authentication method name
 *
 * @return authentication method or NAUTHENTICATION_METHODS
 */
NAuthenticationMethod NDeviceCommon_Authentication_NAuthenticationMethod_FindAuthentication( const char *authenticationMethodName );

#ifdef NDEVICECOMMON_AUTHENTICATION_NAUTHENTICATIONMETHOD_INTERNE
static const char NAuthenticationMethodText[ NAUTHENTICATION_METHODS ][ 32 ] =
{
	"None",

	"Password",
	"PrivateKey",

	"Token"
};
#endif // NDEVICECOMMON_AUTHENTICATION_NAUTHENTICATIONMETHOD_INTERNE

#endif // !NDEVICECOMMON_AUTHENTICATION_NAUTHENTICATIONMETHOD_PROTECT

