#ifndef NDEVICECOMMON_AUTHENTICATION_NAUTHENTICATIONMANAGER_PROTECT
#define NDEVICECOMMON_AUTHENTICATION_NAUTHENTICATIONMANAGER_PROTECT

// ------------------------------------------------------------
// struct NDeviceCommon::Authentication::NAuthenticationManager
// ------------------------------------------------------------

typedef struct NAuthenticationManager
{
	// Authentication entry list (NListe<NAuthenticationEntry*>)
	NListe *m_authentication;

	// Device type
	NDeviceType m_deviceType;

	// Is persistent?
	NBOOL m_isPersistent;
} NAuthenticationManager;

/**
 * Build authentication manager
 *
 * @param deviceType
 * 		The device type
 * @param isPersistent
 * 		Is persistent?
 * @param serviceUsername
 * 		The service configured username
 * @param servicePassword
 * 		The service configured password
 * @param servicePort
 * 		The service listening port
 * @param contactingIP
 * 		The contacting IP
 *
 * @return the authentication manager instance
 */
__ALLOC NAuthenticationManager *NDeviceCommon_Authentication_NAuthenticationManager_Build( NDeviceType deviceType,
	NBOOL isPersistent,
	const char *serviceUsername,
	const char *servicePassword,
	NU32 servicePort,
	const char *contactingIP );

/**
 * Destroy authentication manager
 *
 * @param this
 * 		This instance
 */
void NDeviceCommon_Authentication_NAuthenticationManager_Destroy( NAuthenticationManager** );

/**
 * Activate protection
 *
 * @param this
 * 		This instance
 */
__WILLLOCK void NDeviceCommon_Authentication_NAuthenticationManager_ActivateProtection( NAuthenticationManager* );

/**
 * Deactivate protection
 *
 * @param this
 * 		This instance
 */
__WILLUNLOCK void NDeviceCommon_Authentication_NAuthenticationManager_DeactivateProtection( NAuthenticationManager* );

/**
 * Add authentication
 *
 * @param this
 * 		This instance
 * @param authenticationEntry
 * 		The authentication entry to add
 *
 * @return if operation succeeded
 */
__MUSTBEPROTECTED NBOOL NDeviceCommon_Authentication_NAuthenticationManager_AddAuthentication( NAuthenticationManager*,
	__WILLBEOWNED NAuthenticationEntry *authenticationEntry );

/**
 * Find authentication index by ip and port
 *
 * @param this
 * 		This instance
 * @param ip
 * 		The ip
 * @param port
 * 		The port
 *
 * @return the authentication entry index or NERREUR
 */
__MUSTBEPROTECTED NU32 NDeviceCommon_Authentication_NAuthenticationManager_FindAuthenticationEntryIndexByIP( const NAuthenticationManager*,
	const char *ip,
	NU32 port );

/**
 * Find authentication index by hostname and port
 *
 * @param this
 * 		This instance
 * @param ip
 * 		The ip
 * @param port
 * 		The port
 *
 * @return the authentication entry index or NERREUR
 */
__MUSTBEPROTECTED NU32 NDeviceCommon_Authentication_NAuthenticationManager_FindAuthenticationEntryIndexByHostname( const NAuthenticationManager*,
	const char *hostname,
	NU32 port );

/**
 * Find authentication by ip and port
 *
 * @param this
 * 		This instance
 * @param ip
 * 		The authentication ip to find
 * @param port
 * 		The authentication port to find
 *
 * @return the authentication entry or NULL
 */
__MUSTBEPROTECTED const NAuthenticationEntry *NDeviceCommon_Authentication_NAuthenticationManager_FindAuthenticationForIPAndPort( const NAuthenticationManager*,
	const char *ip,
	NU32 port );

/**
 * Find authentication by hostname and port
 *
 * @param this
 * 		This instance
 * @param hostname
 * 		The authentication ip to find
 * @param port
 * 		The authentication port to find
 *
 * @return the authentication entry or NULL
 */
__MUSTBEPROTECTED const NAuthenticationEntry *NDeviceCommon_Authentication_NAuthenticationManager_FindAuthenticationForHostnameAndPort( const NAuthenticationManager*,
	const char *hostname,
	NU32 port );

/**
 * Get authentication count
 *
 * @param this
 * 		This instance
 *
 * @return the authentication entry count
 */
__MUSTBEPROTECTED NU32 NDeviceCommon_Authentication_NAuthenticationManager_GetAuthenticationCount( const NAuthenticationManager* );

/**
 * Get authentication by index
 *
 * @param this
 * 		This instance
 * @param index
 * 		The index
 *
 * @return the authentication at %index%
 */
__MUSTBEPROTECTED const NAuthenticationEntry *NDeviceCommon_Authentication_NAuthenticationManager_GetAuthenticationByIndex( const NAuthenticationManager*,
	NU32 index );

/**
 * Remove authentication by ip and port
 *
 * @param this
 * 		This instance
 * @param ip
 * 		The authentication ip
 * @param port
 * 		The port
 *
 * @return if operation succeeded
 */
__MUSTBEPROTECTED NBOOL NDeviceCommon_Authentication_NAuthenticationManager_RemoveAuthenticationByIPAndPort( NAuthenticationManager*,
	const char *ip,
	NU32 port );

/**
 * Remove authentication by hostname and port
 *
 * @param this
 * 		This instance
 * @param hostname
 * 		The authentication hostname
 * @param port
 * 		The port
 *
 * @return if operation succeeded
 */
__MUSTBEPROTECTED NBOOL NDeviceCommon_Authentication_NAuthenticationManager_RemoveAuthenticationByHostnameAndPort( NAuthenticationManager*,
	const char *hostname,
	NU32 port );

/**
 * Remove authentication by index
 *
 * @param this
 * 		This instance
 * @param index
 * 		The index
 *
 * @return if operation succeeded
 */
__MUSTBEPROTECTED NBOOL NDeviceCommon_Authentication_NAuthenticationManager_RemoveAuthenticationByIndex( NAuthenticationManager*,
	NU32 index );

/**
 * Process REST request
 *
 * @param this
 * 		This instance
 * @param requestedElement
 * 		The requested element
 * @param requestType
 * 		The http request type
 * @param data
 * 		The request data
 * @param dataLength
 * 		The data length
 * @param clientID
 * 		The client ID
 *
 * @return the http response
 */
__ALLOC __WILLLOCK __WILLUNLOCK NReponseHTTP *NDeviceCommon_Authentication_NAuthenticationManager_ProcessRESTRequest( NAuthenticationManager*,
	const char *requestedElement,
	NTypeRequeteHTTP requestType,
	const char *data,
	NU32 dataLength,
	NU32 clientID );

/**
 * Save authentication details
 *
 * @param this
 * 		This instance
 *
 * @return if operation succeeded
 */
__MUSTBEPROTECTED NBOOL NDeviceCommon_Authentication_NAuthenticationManager_Save( const NAuthenticationManager* );

/**
 * Send all known authentication details
 *
 * @param this
 * 		This instance
 * @param httpClient
 * 		The http client where to send
 * @param authorizationToken
 * 		The authorization token
 *
 * @return if operation succeeded
 */
__WILLLOCK __WILLUNLOCK NBOOL NDeviceCommon_Authentication_NAuthenticationManager_SendAllAuthentication( const NAuthenticationManager*,
	NClientHTTP *httpClient,
	const char *authorizationToken );

#endif // !NDEVICECOMMON_AUTHENTICATION_NAUTHENTICATIONMANAGER_PROTECT
