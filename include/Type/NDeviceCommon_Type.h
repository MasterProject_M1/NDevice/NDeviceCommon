#ifndef NDEVICECOMMON_TYPE_PROTECT
#define NDEVICECOMMON_TYPE_PROTECT

// -----------------------------
// namespace NDeviceCommon::Type
// -----------------------------

// namespace NDeviceCommon::Type::Ghost
#include "Ghost/NDeviceCommon_Type_Ghost.h"

// namespace NDeviceCommon::Type::HUE
#include "HUE/NDeviceCommon_Type_HUE.h"

// namepsace NDeviceCommon::Type::MFI
#include "MFI/NDeviceCommon_Type_MFI.h"

// enum NDeviceCommon::Type::NDeviceType
#include "NDeviceCommon_Type_NDeviceType.h"

#endif // !NDEVICECOMMON_TYPE_PROTECT
