#ifndef NDEVICECOMMON_TYPE_MFI_PROTECT
#define NDEVICECOMMON_TYPE_MFI_PROTECT

// ----------------------------------
// namespace NDeviceCommon::Type::MFI
// ----------------------------------

// Listening port
#define NDEVICE_COMMON_TYPE_MFI_LISTENING_PORT				80

// Default name
#define NDEVICE_COMMON_TYPE_MFI_DEFAULT_NAME				"mFi mPower"

// Location part to look for
#define NDEVICE_COMMON_TYPE_MFI_LOCATION_PART				"/cookiechecker?uri=/mfi/ping.cgi"

// Lifecheck file
#define NDEVICE_COMMON_TYPE_MFI_LIFECHECK					"/mfi/ping.cgi"

// Lifecheck expected answer
#define NDEVICE_COMMON_TYPE_MFI_LIFECHECK_EXPECTED_ANSWER	"{\"rsp\":\"ack\"}"

// Cookie part to look for
#define NDEVICE_COMMON_TYPE_MFI_COOKIE_PART					"AIROS_SESSIONID="

#endif // !NDEVICECOMMON_TYPE_MFI_PROTECT
