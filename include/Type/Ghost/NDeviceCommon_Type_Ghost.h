#ifndef NDEVICECOMMON_TYPE_GHOST_PROTECT
#define NDEVICECOMMON_TYPE_GHOST_PROTECT

// ------------------------------------
// namespace NDeviceCommon::Type::Ghost
// ------------------------------------

// Listening port
#define NDEVICE_COMMON_TYPE_GHOST_CLIENT_LISTENING_PORT			16560
#define NDEVICE_COMMON_TYPE_GHOST_AUDIO_LISTENING_PORT			16561
#define NDEVICE_COMMON_TYPE_GHOST_RFID_LISTENING_PORT			16562
#define NDEVICE_COMMON_TYPE_GHOST_WATCHER_LISTENING_PORT		16563
#define NDEVICE_COMMON_TYPE_GHOST_DOOR_OPENER					16564
#define NDEVICE_COMMON_TYPE_GHOST_AI_INTERFACE					16565

// Auto light state update delay (ms)
#define NDEVICE_COMMON_TYPE_HUE_AUTO_LIGHT_UPDATE_DELAY			1000

// Default timeout for connection attempt
#define NDEVICE_COMMON_GHOST_CONNECTION_DEFAULT_TIMEOUT			5000

// Life check
#define NDEVICE_COMMON_TYPE_GHOST_LIFECHECK					"/ghost/info"

// Status
#define NDEVICE_COMMON_TYPE_GHOST_STATUS					"/ghost/status/"

// Authentication manager
#define NDEVICE_COMMON_TYPE_GHOST_AUTHENTICATION			"/ghost/authentication/"

// Token manager
#define NDEVICE_COMMON_TYPE_GHOST_TOKEN_MANAGER				"/ghost/token"

// Audio service
#define NDEVICE_COMMON_TYPE_GHOST_AUDIO_AUDIO_SERVICE		"/ghost/audio/"

// Action service
#define NDEVICE_COMMON_TYPE_GHOST_ACTION_SERVICE			"/ghost/action/"

// Door opening url
#define NDEVICE_COMMON_TYPE_GHOST_DOOR_OPENER_URI			"/ghost/door/open"

// Event
#define NDEVICE_COMMON_TYPE_GHOST_WATCHER_EVENT_SERVICE		"/ghost/event/"

// UUID key
#define NDEVICE_COMMON_TYPE_GHOST_UUID_KEY					"device.uuid"

// Your IP key
#define NDEVICE_COMMON_TYPE_GHOST_YOUR_IP_KEY				"yourIP"

// Type key
#define NDEVICE_COMMON_TYPE_GHOST_DEVICE_TYPE				"device.type"

// Name key
#define NDEVICE_COMMON_TYPE_GHOST_DEVICE_NAME				"device.name"

// Unauthorized message
#define NDEVICE_COMMON_TYPE_GHOST_UNAUTHORIZED_MESSAGE		"You are not authorized to access this service\n"

// Internal error
#define NDEVICE_COMMON_TYPE_GHOST_INTERNAL_ERROR_MESSAGE	"An internal error occured\n"

// Not implemented
#define NDEVICE_COMMON_TYPE_GHOST_NOT_IMPLEMENTED_MESSAGE	"This is not implemented\n"

// Bad request
#define NDEVICE_COMMON_TYPE_GHOST_BAD_REQUEST_MESSAGE		"Bad request\n"

// OK
#define NDEVICE_COMMON_TYPE_GHOST_OK_MESSAGE				"OK\n"

// Empty
#define NDEVICE_COMMON_TYPE_GHOST_EMPTY_MESSAGE				"\n"

// Device is not initializated
#define NDEVICE_COMMON_TYPE_GHOST_NOT_INITIALIZATED			"Device is not ready\n"

#endif // !NDEVICECOMMON_TYPE_GHOST_PROTECT
