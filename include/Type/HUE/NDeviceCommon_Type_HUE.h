#ifndef NDEVICECOMMON_TYPE_HUE_PROTECT
#define NDEVICECOMMON_TYPE_HUE_PROTECT

// ----------------------------------
// namespace NDeviceCommon::Type::HUE
// ----------------------------------

// Listening port
#define NDEVICE_COMMON_TYPE_HUE_LISTENING_PORT		80

// Life check
#define NDEVICE_COMMON_TYPE_HUE_LIFECHECK			"/api/config"

// HUE keys
#define NDEVICE_COMMON_TYPE_HUE_NAME_KEY			"name"
#define NDEVICE_COMMON_TYPE_CORRECT_HUE_NAME		"Philips hue"

#define NDEVICE_COMMON_TYPE_HUE_UUID_KEY			"bridgeid"

// HUE database
#define NDEVICEHUE_DATABASE_LOCATION				"HUE"

// Hue name
#define NDEVICE_COMMON_TYPE_HUE_DEFAULT_NAME		"HUE Hub"

// HUE waiting time for id obtention
#define NDEVICEHUE_WAITING_TIME_BETWEEN_ID_REQUEST	2000

// HUE URL to get new ID
#define NDEVICEHUE_URL_TO_GET_NEW_ID				"/api"

// Default user
#define NDEVICEHUE_DEFAULT_DEVICE_USER				"{\"devicetype\":\"GhostUser\"}"

// User light obtention
#define NDEVICEHUE_LIGHT_LIST_API_REQUEST			"/api/%s/lights"

// Id key if success
#define NDEVICEHUE_JSON_SUCCESS_KEY_ID_CREATION		"success.username"

// Light state edit API
#define NDEVICEHUE_LIGHT_STATE_API_REQUEST			"/api/%s/lights/%d/state"

// Light edit API
#define NDEVICEHUE_LIGHT_API_REQUEST				"/api/%s/lights/%d"

// Light request timeout
#define NDEVICEHUE_LIGHT_REQUEST_TIMEOUT			100

// Factor to pass from standard hue value (0-360) to philips HUE hue color value (0-65535)
#define NDEVICEHUE_HUE_CONVERSION_FACTOR			182

#endif // !NDEVICECOMMON_TYPE_HUE_PROTECT
