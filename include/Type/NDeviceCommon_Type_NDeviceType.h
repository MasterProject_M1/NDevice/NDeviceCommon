#ifndef NDEVICECOMMON_TYPE_NDEVICETYPE_PROTECT
#define NDEVICECOMMON_TYPE_NDEVICETYPE_PROTECT

// -------------------------------------
// enum NDeviceCommon::Type::NDeviceType
// -------------------------------------

typedef enum NDeviceType
{
	NDEVICE_TYPE_HUE,
	NDEVICE_TYPE_MFI_MPOWER,

	NDEVICE_TYPE_GHOST_CLIENT,
	NDEVICE_TYPE_GHOST_AUDIO,
	NDEVICE_TYPE_GHOST_RFID,
	NDEVICE_TYPE_GHOST_WATCHER,
	NDEVICE_TYPE_GHOST_DOOR_OPENER,
	NDEVICE_TYPE_GHOST_AI_INTERFACE,

	NDEVICES_TYPE
} NDeviceType;

/**
 * Is it a ghost device?
 *
 * @param deviceType
 * 		The device type
 *
 * @return if this is a ghost device
 */
NBOOL NDeviceCommon_Type_NDeviceType_IsGhostDevice( NDeviceType deviceType );

/**
 * Get device user friendly type name
 *
 * @param deviceType
 * 		The device type
 *
 * @return the device type name
 */
const char *NDeviceCommon_Type_NDeviceType_GetUserFriendlyName( NDeviceType deviceType );

/**
 * Get device formal name
 *
 * @param deviceType
 * 		The device type
 *
 * @return the formal name
 */
const char *NDeviceCommon_Type_NDeviceType_GetName( NDeviceType deviceType );

/**
 * Get device listening port
 *
 * @param deviceType
 * 		The device type
 *
 * @return the device listening port
 */
NU32 NDeviceCommon_Type_NDeviceType_GetListeningPort( NDeviceType deviceType );

/**
 * Get element to GET to check if a device has specified type
 *
 * @param deviceType
 * 		The device type
 *
 * @return the GET element
 */
const char *NDeviceCommon_Type_NDeviceType_GetElementToGET( NDeviceType deviceType );

/**
 * Find device through type name (case insensitive)
 *
 * @param typeName
 * 		The type name
 *
 * @return the device type
 */
NDeviceType NDeviceCommon_Type_NDeviceType_FindTypeFromName( const char *typeName );

/**
 * Get device token header
 *
 * @param deviceType
 * 		The device type
 *
 * @return the short header for token
 */
const char *NDeviceCommon_Type_NDeviceType_GetShortHeaderName( NDeviceType deviceType );

/**
 * Get device type id modifier
 *
 * @param deviceType
 * 		The device type
 *
 * @return the ghost device modifier
 */
NU64 NDeviceCommon_Type_NDeviceType_GetIdModifier( NDeviceType deviceType );

#ifdef NDEVICECOMMON_TYPE_NDEVICETYPE_INTERNE
__PRIVATE const char NDeviceTypeUserFriendlyName[ NDEVICES_TYPE ][ 32 ] =
{
	"Philips HUE",
	"mFi mPower",
	"Ghost client",
	"Ghost audio",
	"Ghost RFID",
	"Ghost watcher",
	"Ghost door opener",
	"Ghost AI interface"
};

__PRIVATE const char NDeviceTypeTokenHeaderName[ NDEVICES_TYPE ][ 32 ] =
{
	"HUE ",
	"MFIM",
	"GHOC",
	"GHOA",
	"GRFD",
	"GWTC",
	"GDOP",
	"GAII"
};

__PRIVATE const char NDeviceTypeName[ NDEVICES_TYPE ][ 32 ] =
{
	"Hue",
	"MFI",
	"GhostClient",
	"GhostAudio",
	"GhostRFID",
	"GhostWatcher",
	"GhostDoorOpener",
	"GhostAIInterface"
};

__PRIVATE const NU32 NDeviceTypeListeningPort[ NDEVICES_TYPE ] =
{
	NDEVICE_COMMON_TYPE_HUE_LISTENING_PORT,
	NDEVICE_COMMON_TYPE_MFI_LISTENING_PORT,
	NDEVICE_COMMON_TYPE_GHOST_CLIENT_LISTENING_PORT,
	NDEVICE_COMMON_TYPE_GHOST_AUDIO_LISTENING_PORT,
	NDEVICE_COMMON_TYPE_GHOST_RFID_LISTENING_PORT,
	NDEVICE_COMMON_TYPE_GHOST_WATCHER_LISTENING_PORT,
	NDEVICE_COMMON_TYPE_GHOST_DOOR_OPENER,
	NDEVICE_COMMON_TYPE_GHOST_AI_INTERFACE
};

__PRIVATE const char NDeviceTypeElementGET[ NDEVICES_TYPE ][ 32 ] =
{
	NDEVICE_COMMON_TYPE_HUE_LIFECHECK,
	NDEVICE_COMMON_TYPE_MFI_LIFECHECK,
	NDEVICE_COMMON_TYPE_GHOST_LIFECHECK,
	NDEVICE_COMMON_TYPE_GHOST_LIFECHECK,
	NDEVICE_COMMON_TYPE_GHOST_LIFECHECK,
	NDEVICE_COMMON_TYPE_GHOST_LIFECHECK,
	NDEVICE_COMMON_TYPE_GHOST_LIFECHECK,
	NDEVICE_COMMON_TYPE_GHOST_LIFECHECK
};

__PRIVATE const NU64 NDeviceTypeIDModifier[ NDEVICES_TYPE ] =
{
	0x0000000000000000,
	0x0000000000000000,
	0x1234123412341234,
	0x4321432143214321,
	0x5678567856785678,
	0x8765876587658765,
	0x1919312345638174,
	0x5678545678876569
};
#endif // NDEVICECOMMON_TYPE_NDEVICETYPE_INTERNE

#endif // !NDEVICECOMMON_TYPE_NDEVICETYPE_PROTECT
