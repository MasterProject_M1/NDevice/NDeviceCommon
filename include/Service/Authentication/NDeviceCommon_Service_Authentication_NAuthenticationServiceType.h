#ifndef NDEVICECOMMON_SERVICE_AUTHENTICATION_NAUTHENTICATIONSERVICETYPE_PROTECT
#define NDEVICECOMMON_SERVICE_AUTHENTICATION_NAUTHENTICATIONSERVICETYPE_PROTECT

// -----------------------------------------------------------------------
// enum NDeviceCommon::Service::Authentication::NAuthenticationServiceType
// -----------------------------------------------------------------------

typedef enum NAuthenticationServiceType
{
	NAUTHENTICATION_SERVICE_TYPE_ROOT,

	NAUTHENTICATION_SERVICE_TYPE_COUNT,
	NAUTHENTICATION_SERVICE_TYPE_AUTHENTICATION_ENTRY,

	NAUTHENTICATION_SERVICE_TYPES
} NAuthenticationServiceType;

/**
 * Get service name
 *
 * @param serviceType
 * 		The service type
 *
 * @return the service name
 */
const char *NDeviceCommon_Service_Authentication_NAuthenticationServiceType_GetName( NAuthenticationServiceType serviceType );

/**
 * Find service type
 *
 * @param authenticationManager
 * 		The authentication manager
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in reeuqested element
 * @param authenticationEntryIndex
 * 		The authentication entry index (output)
 *
 * @return the service type
 */
NAuthenticationServiceType NDeviceCommon_Service_Authentication_NAuthenticationServiceType_FindServiceType( __MUSTBEPROTECTED NAuthenticationManager *authenticationManager,
	const char *requestedElement,
	NU32 *cursor,
	__OUTPUT NU32 *authenticationEntryIndex );

#ifdef NDEVICECOMMON_SERVICE_AUTHENTICATION_NAUTHENTICATIONSERVICETYPE_INTERNE
static const char NAuthenticationServiceTypeText[ NAUTHENTICATION_SERVICE_TYPES ][ 32 ] =
{
	"",

	"count",

	"%s:%d"
};
#endif // NDEVICECOMMON_SERVICE_AUTHENTICATION_NAUTHENTICATIONSERVICETYPE_INTERNE

#endif // !NDEVICECOMMON_SERVICE_AUTHENTICATION_NAUTHENTICATIONSERVICETYPE_PROTECT
