#ifndef NDEVICECOMMON_SERVICE_AUTHENTICATION_PROTECT
#define NDEVICECOMMON_SERVICE_AUTHENTICATION_PROTECT

// ------------------------------------------------
// namespace NDeviceCommon::Service::Authentication
// ------------------------------------------------

// enum NDeviceCommon::Service::Authentication::NAuthenticationEntryServiceType
#include "NDeviceCommon_Service_Authentication_NAuthenticationEntryServiceType.h"

// enum NDeviceCommon::Service::Authentication::NAuthenticationServiceType
#include "NDeviceCommon_Service_Authentication_NAuthenticationServiceType.h"

/**
 * Extract data from user request for authentication entry
 *
 * @param parserOutputList
 * 		The parser output list
 * @param root
 * 		The root to consider
 * @param type
 * 		The authentication entry type
 * @param method
 * 		The authentication method
 * @param hostname
 * 		The authentication entry hostname
 * @param user
 * 		The authentication entry user
 * @param privateData
 * 		The authentication entry private data
 * @param isPostRequest
 * 		Is it an extraction for a POST request? (If yes, all the fields must be set)
 *
 * @return if operation succeeded
 */
NBOOL NDeviceCommon_Service_Authentication_ExtractUserDataFromParserOutputList( const NParserOutputList *parserOutputList,
	const char *root,
	__OUTPUT NU32 *port,
	__OUTPUT NAuthenticationEntryType *type,
	__OUTPUT NAuthenticationMethod *method,
	__OUTPUT __ALLOC char **hostname,
	__OUTPUT __ALLOC char **user,
	__OUTPUT __ALLOC char **privateData,
	NBOOL isPostRequest );

/**
 * Process authentication REST POST request
 *
 * @param authenticationManager
 * 		The authentication manager
 * @param clientData
 * 		The client data
 * @param clientDataLength
 * 		The client data length
 *
 * @return if operation succeeded
 */
NBOOL NDeviceCommon_Service_Authentication_ProcessRESTPOSTRequest( __MUSTBEPROTECTED NAuthenticationManager *authenticationManager,
	const char *clientData,
	NU32 clientDataLength );

/**
 * Process authentication REST PUT request
 *
 * @param authenticationManager
 * 		The authentication manager
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 * @param clientData
 * 		The client data
 * @param clientDataLength
 * 		The client data length
 *
 * @return if operation succeeded
 */
NBOOL NDeviceCommon_Service_Authentication_ProcessRESTPUTRequest( __MUSTBEPROTECTED NAuthenticationManager *authenticationManager,
	const char *requestedElement,
	NU32 *cursor,
	const char *clientData,
	NU32 clientDataLength );

/**
 * Process authentication REST GET request
 *
 * @param authenticationManager
 * 		The authentication manager
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 * @param clientID
 * 		The client ID
 *
 * @return the http response
 */
__ALLOC NReponseHTTP *NDeviceCommon_Service_Authentication_ProcessRESTGETRequest( __MUSTBEPROTECTED NAuthenticationManager *authenticationManager,
	const char *requestedElement,
	NU32 *cursor,
	NU32 clientID );

/**
 * Process authentication REST DELETE request
 *
 * @param authenticationManager
 * 		The authentication manager
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 * @param clientID
 * 		The client ID
 *
 * @return if operation succeeded
 */
NBOOL NDeviceCommon_Service_Authentication_ProcessRESTDELETERequest( __MUSTBEPROTECTED NAuthenticationManager *authenticationManager,
	const char *requestedElement,
	NU32 *cursor );

#endif // !NDEVICECOMMON_SERVICE_AUTHENTICATION_PROTECT
