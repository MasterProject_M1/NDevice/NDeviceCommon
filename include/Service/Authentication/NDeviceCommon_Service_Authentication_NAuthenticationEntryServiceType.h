#ifndef NDEVICECOMMON_SERVICE_AUTHENTICATION_NAUTHENTICATIONENTRYSERVICETYPE_PROTECT
#define NDEVICECOMMON_SERVICE_AUTHENTICATION_NAUTHENTICATIONENTRYSERVICETYPE_PROTECT

// ----------------------------------------------------------------------------
// enum NDeviceCommon::Service::Authentication::NAuthenticationEntryServiceType
// ----------------------------------------------------------------------------

typedef enum NAuthenticationEntryServiceType
{
	NAUTHENTICATION_ENTRY_SERVICE_TYPE_ROOT,

	NAUTHENTICATION_ENTRY_SERVICE_TYPE_IP,
	NAUTHENTICATION_ENTRY_SERVICE_TYPE_HOSTNAME,
	NAUTHENTICATION_ENTRY_SERVICE_TYPE_PORT,
	NAUTHENTICATION_ENTRY_SERVICE_TYPE_USER,
	NAUTHENTICATION_ENTRY_SERVICE_TYPE_TYPE,
	NAUTHENTICATION_ENTRY_SERVICE_TYPE_METHOD,
	NAUTHENTICATION_ENTRY_SERVICE_TYPE_PRIVATE_DATA,

	NAUTHENTICATION_ENTRY_SERVICE_TYPE_IS_READ_ONLY,

	NAUTHENTICATION_ENTRY_SERVICE_TYPES
} NAuthenticationEntryServiceType;

/**
 * Get service name
 *
 * @param serviceType
 * 		The service type
 *
 * @return the service name
 */
const char *NDeviceCommon_Service_Authentication_NAuthenticationEntryServiceType_GetName( NAuthenticationEntryServiceType serviceType );

/**
 * Find service type
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		Cursor in the requested element
 *
 * @return the service type or NAUTHENTICATION_ENTRY_SERVICE_TYPES
 */
NAuthenticationEntryServiceType NDeviceCommon_Service_Authentication_NAuthenticationEntryServiceType_FindServiceType( const char *requestedElement,
	NU32 *cursor );

#ifdef NDEVICECOMMON_SERVICE_AUTHENTICATION_NAUTHENTICATIONENTRYSERVICETYPE_INTERNE
static const char NAuthenticationEntryServiceTypeText[ NAUTHENTICATION_ENTRY_SERVICE_TYPES ][ 32 ] =
{
	"",

	"ip",
	"hostname",
	"port",
	"user",
	"type",
	"method",
	"privateData",
	"isReadOnly"
};
#endif // NDEVICECOMMON_SERVICE_AUTHENTICATION_NAUTHENTICATIONENTRYSERVICETYPE_INTERNE

#endif // !NDEVICECOMMON_SERVICE_AUTHENTICATION_NAUTHENTICATIONENTRYSERVICETYPE_PROTECT
