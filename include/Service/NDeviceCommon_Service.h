#ifndef NDEVICECOMMON_SERVICE_PROTECT
#define NDEVICECOMMON_SERVICE_PROTECT

// --------------------------------
// namespace NDeviceCommon::Service
// --------------------------------

// namespace NDeviceCommon::Service::Authentication
#include "Authentication/NDeviceCommon_Service_Authentication.h"

/**
 * Build http response with parser output list
 * Will replace 0xFF by '.'
 *
 * @param httpResponseCode
 * 		The http response code
 * @param parserOutputList
 * 		The parser output list
 * @param clientID
 * 		The client ID
 *
 * @return the http response
 */
__ALLOC NReponseHTTP *NDeviceCommon_Service_GenerateJsonHTTPResponseWithParserOutputList( NHTTPCode httpResponseCode,
	__WILLBEOWNED NParserOutputList *parserOutputList,
	NU32 clientID );

#endif // !NDEVICECOMMON_SERVICE_PROTECT
