#ifndef NDEVICECOMMON_UUID_PROTECT
#define NDEVICECOMMON_UUID_PROTECT

// -----------------------------
// namespace NDeviceCommon::UUID
// -----------------------------

/**
 * Convert uuid to number
 *
 * @param uuid
 * 		The uuid
 *
 * @return the converted uuid or 0xFFFFFFFFFFFFFFFF
 */
NU64 NDeviceCommon_UUID_ConvertUUIDToNumber( const char *uuid );

/**
 * Convert number to uuid
 *
 * @param number
 * 		The number to convert
 *
 * @return the uuid or NULL
 */
__ALLOC char *NDeviceCommon_UUID_ConvertNumberToUUID( NU64 number );

#endif // !NDEVICECOMMON_UUID_PROTECT
