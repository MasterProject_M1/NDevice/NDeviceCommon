NDevice Common
==============

Introduction
------------

Here are all the data related to all devices types

To consider a device of a certain type, it will have
to respond 200 on specified URL, that's how it will be
identified.

Dependencies
------------

- NLib
- NHTTP
- NParser
- NJson

Authentication manager
----------------------

The authentication manager is relative to all devices, and provides authentication
storage for remote services like HTTP basic authentication, SFTP authentication, etc.

**Add authentication entry:**

*To add an authentication entry with POST method, send the following data block*

```json
{
    "hostname": "nproject.ddns.net",
    "port": 16560,
    "user": "userHTTP",
    "type": "HTTPBasic",
    "method": "Password",
    "privateData": "MotDePasse"
}
```
Example:

```bash
curl test:test@127.0.0.1:16560/ghost/authentication/ -X POST -d "{\"hostname\": \"nproject.ddns.net\", \"port\": 16560, \"user\": \"pi\", \"type\": \"HTTPBasic\", \"method\": \"Password\", \"privateData\": \"MotDePasse\" }"
```

Once sent, a new field `ip` will appear if add succeed.

|Field|Remarks|Possible values|
|-----|-------|---------------|
|`type`| |`HTTPBasic`, `SSH/SFTP`, `GhostToken`|
|`method`| |`Password`, `None`, `PrivateKey`|
|`privateData`|Not needed if `None` method specified, can be either password or private key|

**List authentication entries**

```json
{
   "nproject.ddns.net:16560":
   {
       "hostname": "nproject.ddns.net",
       "ip": "192.168.0.1",
       "port": 16560,
       "user": "userHTTP",
       "type": "HTTPBasic",
       "method": "Password",
       "privateData": "MotDePasse"
   },
   "nproject.ddns.net:16561":
   {
       "hostname": "nproject.ddns.net",
       "ip": "192.168.0.1",
       "port": 16561,
       "user": "userSSH",
       "type": "SSH/SFTP",
       "method": "PrivateKey",
       "privateData": "RSA PRIVATE KEY WITH HEADER AND FOOTER"
   },
   "nproject.ddns.net:16562":
   {
       "hostname": "nproject.ddns.net",
       "ip": "192.168.0.1",
       "port": 16562,
       "user": "userHTTPNoAuth",
       "type": "HTTPBasic",
       "method": "None"
   }
}
```

Devices types
-------------

- Ghost : The ghost project devices, will respond on /ghost/info
- HUE : The philips HUE devices, will respond on /api/config

Author
------

SOARES Lucas <lucas.soares.npro@gmail.com>

https://gitlab.com/MasterProject_M1/Device/NDeviceCommon

